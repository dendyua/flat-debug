
#include <iostream>

#include "../../cpp/Macros.h"

//FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(foo, "C:/prefix/some/path/", true)
//FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(bar, "C:/prefix/some/paeth/", true)

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(ms, "C:\\projects\\ecstatica\\src\\ide\\msvc-th\\..\\..\\code", true)




int main(int, char **)
{
//	constexpr Flat::Debug::DebugDump dump = Flat::Debug::debug_dump_for_match<typename Flat::Debug::BestMatch<-1, \
//			FLAT_DEBUG_TEMPLATE_ARGS_STRING("c:\\preFix/somE/../sOme/paeth/c")>::type>();

	constexpr Flat::Debug::DebugDump dump = Flat::Debug::debug_dump_for_match<typename Flat::Debug::BestMatch<-1, \
			FLAT_DEBUG_TEMPLATE_ARGS_STRING("c:\\projects\\ecstatica\\src\\code\\file.cpp")>::type>();

	std::cout << "name=" << dump.name << " prefix=" << dump.prefixSize << std::endl;

	static constexpr int len = Flat::Debug::path_length("C:\\projects\\ecstatica\\src\\ide\\msvc-th\\..\\..\\code");
	// C:\projects\ecstatica\src\ide\msvc-th\..\..\code
	// C:\projects\ecstatica\src\code
	std::cout << "len:" << len;

	return 0;
}
