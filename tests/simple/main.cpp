
#include <Flat/Debug/Log.hpp>
#include <Flat/Debug/Debug.hpp>

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(Main, SOURCE_DIR, true)

int main(int argc, char**argv)
{
	Flat::Debug::Log log("yo", Flat::Debug::Log::Priority::Warning, Flat::Debug::Log::OutputMode_StdOut);
	log << "bzzzz!" << SOURCE_DIR;
	log.flush();

	FLAT_DEBUG << "Something";

	FLAT_PLAIN_ASSERT(argc == 1);

	try {
		FLAT_CHECK_ERROR(argc == 5) << "argc != 5" << argc;
	} catch ( const Flat::Debug::Exception & e ) {
		FLAT_DEBUG << "Got:" << e.what();
	}

	return 0;
}
