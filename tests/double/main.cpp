
#include <Flat/Debug/Debug.hpp>

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(FlatDebugTestDouble, SOURCE_DIR "/first", true)
FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(FlatDebugTestDouble, 1, SOURCE_DIR "/second")
FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(FlatDebugTestDouble, 2, SOURCE_DIR)

#include "first/a.hpp"
#include "second/b.hpp"

int main(int, char **)
{
	a();
	b();

	FLAT_DEBUG;

	return 0;
}
