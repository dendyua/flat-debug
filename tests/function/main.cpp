
#include <array>
#include <string_view>

#include <Flat/Debug/Debug.hpp>

FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(main, SOURCE_DIR, true)




#ifdef _MSC_VER
#define FUNC_INFO __FUNCSIG__
#else
#define FUNC_INFO __PRETTY_FUNCTION__
#endif




typedef void(*Ret)();
typedef int(*Ret2)(float);
typedef Ret(*Ret3)(float);

struct Foo {
	const bool logdtor;

	Foo(bool l) : logdtor(l) {}

	~Foo() {
		if (logdtor) {
			FLAT_DEBUG << "dtor" << FUNC_INFO;
		}
	}

	void retv() {
		FLAT_DEBUG << "retv" << FUNC_INFO;
	}

	int reti() {
		FLAT_DEBUG << "reti" << FUNC_INFO;
		return 0;
	}

	static int sreti() {
		FLAT_DEBUG << "sreti" << FUNC_INFO;
		return 0;
	}

	Ret retf() {
		FLAT_DEBUG << "retf" << FUNC_INFO;
		return nullptr;
	}

	Ret retbar() {
		struct Bar {
			static Ret3 ret() {
				FLAT_DEBUG << "bar" << FUNC_INFO;
				return nullptr;
			}
		};
		Bar::ret();
		return nullptr;
	}

	void operator()() {
		FLAT_DEBUG << "oppar" << FUNC_INFO;
	}

	void operator+() {
		FLAT_DEBUG << "op+" << FUNC_INFO;
	}
};

void operator<<(const Foo&, const Foo&) {
	FLAT_DEBUG << "op<<" << FUNC_INFO;
}




template <typename T>
constexpr T invalid_throw() {
	throw std::logic_error("");
}




constexpr std::string_view unspace_func_it(const std::string_view & originalString,
		const char * const currentSpace, const std::string_view & currentString) noexcept
{
	if (currentSpace[0] == '\0') {
		// namespace found, removing ok
		return currentString;
	}

	if (currentString.empty()) {
		// string is shorter than namespace, return original full string
		return originalString;
	}

	if (currentSpace[0] == currentString[0]) {
		// keep chopping
		return unspace_func_it(originalString, currentSpace + 1, currentString.substr(1));
	}

	// char mismatch, return original full string
	return originalString;
}


constexpr std::string_view unspace_func(const std::string_view & s,
		const char * const space) noexcept
{
	if (!space) return s;
	return unspace_func_it(s, space, s);
}




constexpr int find_char_fw_it(const char * const s, const char ch, const int from, const int i)
{
	if (s[from + i] == 0) return -1;
	if (s[from + i] == ch) return from + i;
	return find_char_fw_it(s, ch, from, i + 1);
}


constexpr int find_char_fw(const char * const s, const char ch, const int from)
{
	return find_char_fw_it(s, ch, from, 0);
}


constexpr int find_char_bw_it(const char * const s, const char ch, const int from, const int i)
{
	if (from == i) return -1;
	if (s[from - i - 1] == ch) return from - i - 1;
	return find_char_bw_it(s, ch, from, i + 1);
}


constexpr int find_char_bw(const char * const s, const char ch, const int from)
{
	return find_char_bw_it(s, ch, from, 0);
}


constexpr int skip_string_fw_or_nothing_it(const char * const s, const char * const what, const int from, const int i)
{
	if (what[i] == 0) return from + i;
	if (what[i] != s[from + i]) return from;
	return skip_string_fw_or_nothing_it(s, what, from, i + 1);
}


constexpr int skip_string_fw_or_nothing(const char * const s, const char * const what, const int from)
{
	return skip_string_fw_or_nothing_it(s, what, from, 0);
}


constexpr int find_string_fw_or_error(const char * const s, const char * const what, const int from)
{
	const int pos = find_char_fw(s, what[0], from);
	if (pos == -1) return -1;
	const int matchPos = skip_string_fw_or_nothing(s, what, pos);
	if (matchPos != pos) return pos;
	return find_string_fw_or_error(s, what, pos + 1);
}


constexpr int skip_spaces_fw(const char * const s, const int from)
{
	if (s[from] != ' ') return from;
	return skip_spaces_fw(s, from + 1);
}


constexpr int skip_spaces_bw(const char * const s, const int from)
{
	if (from == 0) return 0;
	if (s[from - 1] != ' ') return from;
	return skip_spaces_bw(s, from - 1);
}


constexpr int skip_string_bw_or_error_it(const char * const s, const char * const what,
		const int whatLength, const int from, const int i)
{
	if (i == whatLength) return from - whatLength;
	if (from == i) return -1;
	if (what[whatLength - i - 1] != s[from - i - 1]) return -1;
	return skip_string_bw_or_error_it(s, what, whatLength, from, i + 1);
}


constexpr int skip_string_bw_or_error(const char * const s, const char * const what, const int from)
{
	return skip_string_bw_or_error_it(s, what, Flat::stringLength(what), from, 0);
}


constexpr int skip_paren_stars(const char * const s, const int from, const int lastParenPos)
{
	const int parenPos = find_char_fw(s, '(', from);

	if (parenPos == -1) {
		if (lastParenPos == -1) {
			return invalid_throw<int>();
		}

		return lastParenPos;
	}

	const int cdeclPos = skip_string_fw_or_nothing(s, "__cdecl", parenPos);
	const int spacesPos = skip_spaces_fw(s, cdeclPos);
	const int starPos = find_char_fw(s, '*', spacesPos);
	if (starPos == -1) {
		return parenPos;
	}

	return skip_paren_stars(s, starPos + 1, parenPos);
}


struct _OpFindInfo {
	bool empty;
	int pos;
};


constexpr _OpFindInfo find_operator(const char * const s, const int from)
{
	const int skipEmptyOperatorPos = skip_string_bw_or_error(s, "operator", from);
	if (skipEmptyOperatorPos != -1) {
		return {true, skipEmptyOperatorPos};
	}
	const int spacePos = find_char_bw(s, ' ', from);
	if (spacePos == -1) {
		return {false, -1};
	}
	const int skipSpacesPos = skip_spaces_bw(s, spacePos);
	const int skipNormalOperatorPos = skip_string_bw_or_error(s, "operator", skipSpacesPos);
	if (skipNormalOperatorPos == -1) {
		return {false, -1};
	}
	return {false, skipNormalOperatorPos};
}


constexpr std::string_view get_name(const char * const s, const int pos, const int last)
{
	const int namePos = find_char_bw(s, ' ', pos);
	if (namePos == -1) {
		return std::string_view(s, size_t(last));
	} else {
		return std::string_view(s + namePos + 1, size_t(last - namePos - 1));
	}
}


constexpr std::string_view simplify_func(const char * const s)
{
	const int lambdaPos = find_string_fw_or_error(s, "::<", 0);

	const int parenPos = skip_paren_stars(s, 0, -1);

	if (lambdaPos != -1 && lambdaPos < parenPos) {
		return get_name(s, lambdaPos, lambdaPos);
	}

	const int skipSpacesPos = skip_spaces_bw(s, parenPos);

	const auto operatorPos = find_operator(s, skipSpacesPos);

	if (operatorPos.pos != -1) {
		const bool hasEmptyParensAhead = skip_string_fw_or_nothing(s, "()", parenPos) == parenPos + 2;
		if (operatorPos.empty) {
			if (!hasEmptyParensAhead) return invalid_throw<std::string_view>();
		}
		const int suffixSize = hasEmptyParensAhead ? 2 : 0;
		return get_name(s, operatorPos.pos, parenPos + suffixSize);
	}

	return get_name(s, skipSpacesPos, parenPos);
}




struct Test {
	const std::string_view src;
	const std::string_view exp;
};




int main()
{
	static_assert(find_char_fw("", '(', 0) == -1);
	static_assert(find_char_fw("abc def", '(', 0) == -1);
	static_assert(find_char_fw("abc (def", '(', 0) == 4);
	static_assert(find_char_fw("abc (def", '(', 4) == 4);
	static_assert(find_char_fw("abc (def", '(', 5) == -1);

	static_assert(find_char_bw("", '(', 0) == -1);
	static_assert(find_char_bw("abc def", '(', 3) == -1);
	static_assert(find_char_bw("abc (def", '(', 4) == -1);
	static_assert(find_char_bw("abc (def", '(', 5) == 4);
	static_assert(find_char_bw("abc (def", '(', 5) == 4);
	static_assert(find_char_bw("abc (def", '(', 7) == 4);

	static_assert(skip_string_fw_or_nothing("", "abc", 0) == 0);
	static_assert(skip_string_fw_or_nothing("abc", "abc", 0) == 3);
	static_assert(skip_string_fw_or_nothing("abcdef", "abc", 0) == 3);
	static_assert(skip_string_fw_or_nothing("abcdef", "abc", 1) == 1);
	static_assert(skip_string_fw_or_nothing("abcdef", "bcd", 1) == 4);

	static_assert(skip_string_bw_or_error("", "", 0) == 0);
	static_assert(skip_string_bw_or_error("abc", "", 3) == 3);
	static_assert(skip_string_bw_or_error("abc", "bc", 3) == 1);
	static_assert(skip_string_bw_or_error("abc", "abc", 3) == 0);
	static_assert(skip_string_bw_or_error("abc", "abcd", 3) == -1);

	static_assert(skip_spaces_fw("", 0) == 0);
	static_assert(skip_spaces_fw("   ", 0) == 3);
	static_assert(skip_spaces_fw("   ", 1) == 3);
	static_assert(skip_spaces_fw("   abc", 1) == 3);

	static_assert(skip_spaces_bw("", 0) == 0);
	static_assert(skip_spaces_bw("   ", 2) == 0);
	static_assert(skip_spaces_bw("a  ", 3) == 1);
	static_assert(skip_spaces_bw("abc", 2) == 2);

	constexpr Test kVcSimple{
		"int __cdecl main(void)",
		"main"
	};

	constexpr Test kVcDtor{
		"__cdecl Foo::~Foo(void)",
		"Foo::~Foo"
	};

	constexpr Test kVcDtorNoCdecl{
		"Foo::~Foo(void)",
		"Foo::~Foo"
	};

	constexpr Test kVcOperatorParens{
		"void __cdecl Foo::operator ()(void)",
		"Foo::operator ()"
	};

	constexpr Test kVcOperatorPlus{
		"void __cdecl Foo::operator +(void)",
		"Foo::operator +"
	};

	constexpr Test kVcClassFunc{
		"void __cdecl Foo::retv(void)",
		"Foo::retv"
	};

	constexpr Test kVcFuncRet{
		"void (__cdecl *__cdecl Foo::retf(void))(void)",
		"Foo::retf"
	};

	constexpr Test kVcFuncDoubleRet{
		"void (__cdecl *(__cdecl *__cdecl Foo::retbar::Bar::ret(void))(float))(void)",
		"Foo::retbar::Bar::ret"
	};

	constexpr Test kVcOperatorLShift{
		"void __cdecl operator <<(const struct Foo &,const struct Foo &)",
		"operator <<"
	};

	constexpr Test kVcLambda{
		"auto __cdecl main::<lambda_19ad23cbe4cb7282cc3b22b6206c7fc2>::operator ()(void) const",
		"main"
	};

	static_assert(simplify_func(kVcSimple.src.data()) == kVcSimple.exp);
	static_assert(simplify_func(kVcDtor.src.data()) == kVcDtor.exp);
	static_assert(simplify_func(kVcDtorNoCdecl.src.data()) == kVcDtorNoCdecl.exp);
	static_assert(simplify_func(kVcOperatorParens.src.data()) == kVcOperatorParens.exp);
	static_assert(simplify_func(kVcOperatorPlus.src.data()) == kVcOperatorPlus.exp);
	static_assert(simplify_func(kVcClassFunc.src.data()) == kVcClassFunc.exp);
	static_assert(simplify_func(kVcFuncRet.src.data()) == kVcFuncRet.exp);
	static_assert(simplify_func(kVcFuncDoubleRet.src.data()) == kVcFuncDoubleRet.exp);
	static_assert(simplify_func(kVcOperatorLShift.src.data()) == kVcOperatorLShift.exp);
	static_assert(simplify_func(kVcLambda.src.data()) == kVcLambda.exp);

	constexpr Test kGnuSimple {
		"int main()",
		"main"
	};

	constexpr Test kGnuDtor{
		"Foo::~Foo()",
		"Foo::~Foo"
	};

	constexpr Test kGnuOperatorParens{
		"void Foo::operator()()",
		"Foo::operator()"
	};

	constexpr Test kGnuOperatorPlus{
		"void Foo::operator+()",
		"Foo::operator+"
	};

	constexpr Test kGnuClassFunc{
		"void Foo::retv()",
		"Foo::retv"
	};

	constexpr Test kGnuFuncRet{
		"void (* Foo::retf())()",
		"Foo::retf"
	};

	constexpr Test kGnuFuncDoubleRet{
		"static void (* (* Foo::retbar()::Bar::ret())(float))()",
		"Foo::retbar"
	};

	constexpr Test kGnuOperatorLShift {
		"void operator<<(const Foo&, const Foo&)",
		"operator<<"
	};

	constexpr Test kGnuLambda{
		"main()::<lambda()>",
		"main"
	};

	static_assert(simplify_func(kGnuSimple.src.data()) == kGnuSimple.exp);
	static_assert(simplify_func(kGnuDtor.src.data()) == kGnuDtor.exp);
	static_assert(simplify_func(kGnuOperatorParens.src.data()) == kGnuOperatorParens.exp);
	static_assert(simplify_func(kGnuOperatorPlus.src.data()) == kGnuOperatorPlus.exp);
	static_assert(simplify_func(kGnuClassFunc.src.data()) == kGnuClassFunc.exp);
	static_assert(simplify_func(kGnuFuncRet.src.data()) == kGnuFuncRet.exp);
	static_assert(simplify_func(kGnuFuncDoubleRet.src.data()) == kGnuFuncDoubleRet.exp);
	static_assert(simplify_func(kGnuOperatorLShift.src.data())== kGnuOperatorLShift.exp);
	static_assert(simplify_func(kGnuLambda.src.data()) == kGnuLambda.exp);

	constexpr const char * kSpaceToRemove = "Space::To::Remove::";

	constexpr Test kSpaceLonger {
		"Remove::foo",
		""
	};

	constexpr Test kSpaceMismatch {
		"Space::Bo::Remove::foo",
		""
	};

	constexpr Test kSpaceGood {
		"Space::To::Remove::foo",
		"foo"
	};

	static_assert(unspace_func(kSpaceLonger.src, kSpaceToRemove) == kSpaceLonger.src);
	static_assert(unspace_func(kSpaceMismatch.src, kSpaceToRemove) == kSpaceMismatch.src);
	static_assert(unspace_func(kSpaceGood.src, kSpaceToRemove) == kSpaceGood.exp);

#if 1
	static constexpr const char * func = FUNC_INFO;
	static constexpr int funcLength = Flat::stringLength(func);
	FLAT_DEBUG << func << funcLength;

	Foo(true);
	Foo(false).operator()();
	Foo(false).operator+();
	Foo(false).retv();
	Foo(false).reti();
	Foo::sreti();
	Foo(false).retf();
	Foo(false).retbar();
	operator<<(Foo(false), Foo(false));

	[] {
		FLAT_DEBUG << "lambda" << FUNC_INFO;
	}();

	struct Bar {
		static void foo() {
			[] {
				FLAT_DEBUG << "sub lambda" << FUNC_INFO;
			}();
		}
	};
	Bar::foo();
#endif

	return 0;
}
