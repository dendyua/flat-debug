
#include "Log.hpp"

#include <algorithm>
#include <cstring>
#include <iostream>

#ifdef __ANDROID__
#include <android/log.h>
#endif




namespace Flat {
namespace Debug {




const char Log::kNullString[] = "<null>";
static_assert(sizeof(Log::kNullString) - 1 == Log::kNullStringSize, "Size mismatch");

Log::OutputCallback * Log::gOutputCallback = nullptr;
Log::FlushCallback * Log::gFlushCallback = nullptr;




#ifdef __ANDROID__
static int toAndroidPriority(const Log::Priority priority)
{
	switch ( priority ) {
	case Log::Priority::Info:    return ANDROID_LOG_INFO;
	case Log::Priority::Warning: return ANDROID_LOG_WARN;
	case Log::Priority::Debug:   return ANDROID_LOG_DEBUG;
	case Log::Priority::Error:   return ANDROID_LOG_ERROR;
	case Log::Priority::Fatal:   return ANDROID_LOG_FATAL;
	}
	FLAT_PLAIN_FATAL;
}
#endif




Log::State::State(Log & log) noexcept :
	log_(log)
{
	putSpace_ = log_.putSpace_;
}


Log::State::~State()
{
	if (putSpace_ && !log_.putSpace_) {
		log_ << Log::Space();
	} else if (!putSpace_ && log_.putSpace_) {
		log_ << Log::NoSpace();
	}
}




Log::OutputCallback * Log::globalOutputCallback() noexcept
{
	return gOutputCallback;
}


void Log::setGlobalOutputCallback(OutputCallback * const callback) noexcept
{
	gOutputCallback = callback;
}


Log::FlushCallback * Log::globalFlushCallback() noexcept
{
	return gFlushCallback;
}


void Log::setGlobalFlushCallback(FlushCallback * const callback) noexcept
{
	gFlushCallback = callback;
}


Log::Log(Log && log) noexcept :
	tag_(log.tag_),
	priority_(log.priority_),
	outputMode_(log.outputMode_)
{
	std::memcpy(buffer_, log.buffer_, size_t(log.size_ + 1));
	bufferStart_ = log.bufferStart_;
	size_ = log.size_;
	putSpace_ = log.putSpace_;
	autoFlush_ = log.autoFlush_;
	dirty_ = log.dirty_;
	outputCallback_ = log.outputCallback_;
	flushCallback_ = log.flushCallback_;

	log.dirty_ = false;
	log.outputCallback_ = nullptr;
	log.flushCallback_ = nullptr;
}


Log::~Log() noexcept(false)
{
	_flush(true);
}


void Log::_flush(const bool final)
{
	if (priority_ == Priority::Null) {
		return;
	}

	if (!dirty_) {
		return;
	}

	// remove leading space character
	if (putSpace_ && size_ > bufferStart_ && buffer_[size_-1] == ' ') {
		buffer_[size_-1] = 0;
		size_--;
	}

	{
		const bool putEol =
#ifdef __ANDROID__
				true;
#else
				final;
#endif

		if (putEol) {
			// put EOL within buffer instead of (<< std::endl) to avoid output fragmentaion
			buffer_[size_] = '\n';
			buffer_[size_+1] = 0;
		}
	}

	if (gOutputCallback) {
		gOutputCallback->write(*this);
	}

	if (outputCallback_) {
		outputCallback_->write(*this);
	}

#ifdef __ANDROID__
	if (outputMode_.test(OutputMode::Logcat)) {
		__android_log_write(toAndroidPriority(priority_), tag_, buffer_);
	}
#endif

	if (!gOutputCallback && outputMode_.test(OutputMode::StdOut | OutputMode::StdErr)) {
		if (outputMode_.test(OutputMode::StdOut)) {
			std::cout << buffer_;
			std::cout.flush();
		}

		if (outputMode_.test(OutputMode::StdErr)) {
			std::cerr << buffer_;
			std::cerr.flush();
		}
	}

#ifdef __ANDROID__
	size_ = bufferStart_;
#else
	size_ = 0;
#endif
	buffer_[size_] = 0;
	dirty_ = false;

	if (final) {
		if (gFlushCallback) {
			gFlushCallback->flushed(*this);
		}

		if (flushCallback_) {
			flushCallback_->flushed(*this);
		}
	}
}


inline static char byteToChar(int byte) noexcept
{
	if (byte < 10) {
		return '0' + byte;
	} else {
		return 'a' + byte - 10;
	}
}


void Log::_printHexString(const char * const s) noexcept
{
	char buf[65];
	char * b = buf;

	for (const char * ch = s; *ch; ++ch) {
		const int i = int(uint8_t(*ch));
		b[0] = byteToChar(i >> 4);
		b[1] = byteToChar(i & 0xf);

		b += 2;

		if (std::distance(&buf[0], b) == 64) {
			b[0] = 0;
			print(buf);
			b = buf;
		}
	}

	if (std::distance(&buf[0], b) != 0) {
		b[0] = 0;
		print(buf);
	}

	maybeSpace();
}


void Log::print(const char * text, int size) noexcept
{
	FLAT_PLAIN_ASSERT(size >= 0)

	if (!text) {
		text = kNullString;
		size = kNullStringSize;
	}

	if (size == 0) {
		return;
	}

	int sizeLeft = kBufferSize - 1 - size_;

	if (!autoFlush_) {
		if (sizeLeft == 0) {
			return;
		}

		const int sizeToAppend = std::min<int>(size, sizeLeft);
		std::strncat(buffer_ + size_, text, size_t(sizeToAppend));

		size_ += sizeToAppend;
	} else {
		while (size > sizeLeft) {
			const int sizeToAppend = sizeLeft;

			// fill the buffer tail
			std::strncat(buffer_ + size_, text, size_t(sizeToAppend));
			size_ = kBufferSize - 1;
			dirty_ = true;
			_flush(false);

#ifdef __ANDROID__
			// prepend next chunk with delimiter
			static const char kNextChunkString[] = "[...] ";
			std::strncat(buffer_ + size_, kNextChunkString, size_t(kBufferSize - size_));
			size_ += sizeof(kNextChunkString) - 1;
#endif

			text += sizeToAppend;
			size -= sizeToAppend;

			sizeLeft = kBufferSize - 1 - size_;
		}

		// append the tail if any
		if (size != 0) {
			std::strncat(buffer_ + size_, text, size_t(size));
			size_ += size;
		}
	}

	dirty_ = true;
}


void Log::print(const char * const text) noexcept
{
	if (!text) {
		print(kNullString, kNullStringSize);
		return;
	}

	print(text, int(std::strlen(text)));
}


void Log::printf(const char * const format, ...) noexcept
{
	va_list args;
	va_start(args, format);
	char buffer[1024];
	const int size = std::vsnprintf(buffer, sizeof(buffer) - 1, format, args);
	va_end(args);
	print(buffer, size);
}


void Log::vprintf(const char * const format, va_list args) noexcept
{
	char buffer[1024];
	const int size = std::vsnprintf(buffer, sizeof(buffer) - 1, format, args);
	print(buffer, size);
}


void Log::flush()
{
	_flush(true);
}


template <>
void Log::append<Log::Space>(const Space &) noexcept
{
	if (putSpace_) {
		return;
	}

	putSpace_ = true;

	// add extraspace instantly
	maybeSpace();
}


template <>
void Log::append<Log::SpaceOnce>(const SpaceOnce &) noexcept
{
	if (putSpace_) {
		return;
	}

	append<Space>(Space());
	putSpace_ = false;
}


template <>
void Log::append<Log::NoSpace>(const NoSpace &) noexcept
{
	if (!putSpace_) {
		return;
	}

	putSpace_ = false;

	// remove last extra space if any
	if (size_ > bufferStart_ && buffer_[size_-1] == ' ') {
		buffer_[size_-1] = 0;
		size_--;
	}
}


template <>
void Log::append<Log::NoSpaceOnce>(const NoSpaceOnce &) noexcept
{
	if (!putSpace_) {
		return;
	}

	append<NoSpace>(NoSpace());
	putSpace_ = true;
}


template <>
void Log::append<Log::SetBufferStart>(const SetBufferStart &) noexcept
{
	bufferStart_ = size_;
}


template <>
void Log::append<Log::Flush>(const Flush &)
{
	_flush(false);
}


template <>
void Log::append<Log::AutoFlush>(const AutoFlush & autoFlush) noexcept
{
	autoFlush_ = autoFlush.isSet();
}


template <>
void Log::append<Log::SetOutputCallback>(const SetOutputCallback & outputCallback) noexcept
{
	outputCallback_ = outputCallback.callback;
}


template <>
void Log::append<Log::SetFlushCallback>(const SetFlushCallback & flushCallback) noexcept
{
	flushCallback_ = flushCallback.callback;
}




}}
