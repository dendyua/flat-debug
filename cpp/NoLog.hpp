
#pragma once

#include <cstdlib>




namespace Flat {
namespace Debug {




/**
  * Dummy stub to mute all input.
  */

class NoLog
{
public:
	template <typename... T>
	NoLog(const T &...) noexcept {}

	template <typename T>
	NoLog & operator<<(const T &) noexcept { return *this; }

	void flush() noexcept {}
};




class FatalNoLog
{
public:
	template <typename... T>
	FatalNoLog(const T &...) noexcept {}

	[[noreturn]] ~FatalNoLog() { std::abort(); }

	template <typename T>
	FatalNoLog & operator<<(const T &) noexcept { return *this; }
};




}}
