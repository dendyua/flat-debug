
#include "Utils.hpp"

#include <chrono>

#ifdef __linux__
#include <unistd.h>
#include <sys/syscall.h>
#endif




namespace Flat {
namespace Debug {




std::chrono::microseconds current_time() noexcept
{
	auto now = std::chrono::system_clock::now();
	return std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
}


int current_timestamp() noexcept
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(current_time()).count() % 1000000;
}




}}
