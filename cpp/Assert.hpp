
#pragma once

#include "Log.hpp"




namespace Flat {
namespace Debug {




class FLAT_DEBUG_EXPORT AssertLog
{
public:
	struct Condition {};

	AssertLog(bool condition, const char * conditionString, const char * tag,
			Log::Priority priority, Log::OutputMode outputMode) noexcept;
	~AssertLog();

	template <typename T> AssertLog & operator<<(const T & value) noexcept;

private:
	class FlushCallback : public Log::FlushCallback
	{
	public:
		FlushCallback(AssertLog & log) noexcept : log_(log) {}
		void flushed(Log&) override { log_._flushed(); }

	private:
		AssertLog & log_;
	};

private:
	[[noreturn]] void _flushed();

private:
	const bool fail_;
	const char * const conditionString_;
	std::unique_ptr<Log> log_;
	std::shared_ptr<FlushCallback> flushCallback_;

	friend class FlushCallback;
};




inline AssertLog::AssertLog(const bool condition, const char * const conditionString,
		const char * const tag, const Log::Priority priority,
		const Log::OutputMode outputMode) noexcept :
	fail_(!condition),
	conditionString_(conditionString)
{
	if (FLAT_UNLIKELY(fail_)) {
		log_.reset(new Log(tag, priority, outputMode));

		flushCallback_ = std::make_shared<FlushCallback>(*this);
		*log_ << Log::SetFlushCallback(flushCallback_);

		*log_ << "Assert failed: (" << Log::NoSpaceOnce() << conditionString_
				<< Log::NoSpaceOnce() << ")";
	}
}


inline AssertLog::~AssertLog()
{
	if (FLAT_UNLIKELY(fail_)) {
		log_.reset(); // no return
	}
}


template <>
inline AssertLog & AssertLog::operator<< <AssertLog::Condition>(const AssertLog::Condition &) noexcept
{
	if (FLAT_UNLIKELY(fail_)) {
		*log_ << "Assert failed: (" << Log::NoSpaceOnce() << conditionString_
				<< Log::NoSpaceOnce() << ")";
	}

	return *this;
}


template <typename T>
inline AssertLog & AssertLog::operator<<(const T & value) noexcept
{
	if ( FLAT_UNLIKELY(fail_) ) {
		*log_ << value;
	}

	return *this;
}




#define __FLAT_DEBUG_ASSERT_LOG_CONSTRUCT(condition, tag, priority, outputMode) \
	Flat::Debug::AssertLog(static_cast<bool>(condition), #condition, tag, priority, outputMode) << Flat::Debug::Log::NoOp()

#define FLAT_DEBUG_ASSERT_LOG_FATAL(condition, tag, outputMode) \
	__FLAT_DEBUG_ASSERT_LOG_CONSTRUCT(condition, tag, Flat::Debug::Log::Priority::Fatal, outputMode)




}}
