
#pragma once

#include "Offset.hpp"




namespace Flat {
namespace Debug {




class FunctionLogger
{
public:
	FunctionLogger(const char * file, int line, const char * function) noexcept :
		file_(file),
		line_(line),
		function_(function),
		exitLine_(-1)
	{}

	void setExitLine(int line) noexcept { exitLine_ = line; }

	const char * file() const noexcept { return file_; }
	int line() const noexcept { return line_; }
	const char * function() const noexcept { return function_; }
	int exitLine() const noexcept { return exitLine_; }

private:
	const char * const file_;
	const int line_;
	const char * const function_;
	int exitLine_;
};




}}
