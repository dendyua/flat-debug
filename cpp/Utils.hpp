
#pragma once

#include <cstdint>
#include <thread>

#include "Log.hpp"
#include "Offset.hpp"




namespace Flat {
namespace Debug {




FLAT_DEBUG_EXPORT std::chrono::microseconds current_time() noexcept;
FLAT_DEBUG_EXPORT int current_timestamp() noexcept;




inline std::size_t current_thread_id() noexcept
{
	return std::hash<std::thread::id>()(std::this_thread::get_id());
}


inline const char * log_offset() noexcept
{
	return IndentString<2>(Offset::instance()->valueForCurrentThread()).string();
}


inline const char * log_tag() noexcept
{
	return "Flat";
}


inline constexpr Log::OutputMode log_output_mode() noexcept
{
#ifdef __ANDROID__
	return Log::OutputMode::Logcat;
#else
#ifdef FLAT_DEBUG_LOG_OUTPUT_TO_ERROR
    return Log::OutputMode::StdErr;
#else
    return Log::OutputMode::StdOut;
#endif
#endif
}


inline constexpr Log::OutputMode log_error_mode() noexcept
{
#ifdef __ANDROID__
	return Log::OutputMode::Logcat;
#else
	return Log::OutputMode::StdErr;
#endif
}




}}




#ifndef FLAT_DEBUG_LOG_TAG_INDENT
#	define FLAT_DEBUG_LOG_TAG_INDENT 8
#endif

#ifdef _MSC_VER
#	define FLAT_DEBUG_FUNC_INFO __FUNCSIG__
#	error "define FLAT_DEBUG_LAMBDA_PREFIX and FLAT_DEBUG_LAMBDA_SUFFIX"
#else
#	define FLAT_DEBUG_FUNC_INFO __PRETTY_FUNCTION__
#	if defined(__clang__)
#		define FLAT_DEBUG_LAMBDA_PREFIX "auto "
#		define FLAT_DEBUG_LAMBDA_SUFFIX "::(anonymous class)::operator()()"
#	elif defined(__GNUC__)
#		define FLAT_DEBUG_LAMBDA_PREFIX ""
#		define FLAT_DEBUG_LAMBDA_SUFFIX "::<lambda()>"
#	else
#		error "Unknown compiler"
#	endif
#endif

#define FLAT_DEBUG_LOG_STRINGIFY(x) #x
#define FLAT_DEBUG_LOG_STRINGIFY_X(x) FLAT_DEBUG_LOG_STRINGIFY(x)
#define FLAT_DEBUG_LOG_TAG_INDENT_STRING FLAT_DEBUG_LOG_STRINGIFY_X(FLAT_DEBUG_LOG_TAG_INDENT)

#define FLAT_DEBUG_LOG_TIMESTAMP_FORMAT "[%06d] "
#define FLAT_DEBUG_LOG_TIMESTAMP_VALUE  int(Flat::Debug::current_timestamp()),
#define FLAT_DEBUG_LOG_THREAD_FORMAT    "(%08x) "
#define FLAT_DEBUG_LOG_THREAD_VALUE     static_cast<uint32_t>(Flat::Debug::current_thread_id()),
#define FLAT_DEBUG_LOG_OFFSET_FORMAT    "%s"
#define FLAT_DEBUG_LOG_OFFSET_VALUE     Flat::Debug::log_offset(),
#define FLAT_DEBUG_LOG_FUNC_SUFFIX      " -"
#define FLAT_DEBUG_LOG_FILE_LINE_FORMAT "%s:%d"
