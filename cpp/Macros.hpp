
#pragma once

#include "Assert.hpp"
#include "Exception.hpp"
#include "Fatal.hpp"
#include "FunctionLogger.hpp"
#include "Log.hpp"
#include "LogTypesStd.hpp"
#include "PathParser.hpp"
#include "Utils.hpp"




namespace Flat {
namespace Debug {




template <typename T>
constexpr T invalid_throw() {
	throw std::logic_error("");
}




constexpr std::string_view unspace_func_it(const std::string_view & originalString,
		const char * const currentSpace, const std::string_view & currentString) noexcept
{
	if (currentSpace[0] == '\0') {
		// namespace found, removing ok
		return currentString;
	}

	if (currentString.empty()) {
		// string is shorter than namespace, return original full string
		return originalString;
	}

	if (currentSpace[0] == currentString[0]) {
		// keep chopping
		return unspace_func_it(originalString, currentSpace + 1, currentString.substr(1));
	}

	// char mismatch, return original full string
	return originalString;
}


constexpr std::string_view unspace_func(const std::string_view & s,
		const char * const space) noexcept
{
	if (!space) return s;
	return unspace_func_it(s, space, s);
}




constexpr int find_char_fw_it(const std::string_view s, const char ch, const int from, const int i)
{
	if (from + i == int(s.size())) return -1;
	if (s[from + i] == ch) return from + i;
	return find_char_fw_it(s, ch, from, i + 1);
}


constexpr int find_char_fw(const std::string_view s, const char ch, const int from)
{
	return find_char_fw_it(s, ch, from, 0);
}


constexpr int find_char_bw_it(const std::string_view s, const char ch, const int from, const int i)
{
	if (from == i) return -1;
	if (s[from - i - 1] == ch) return from - i - 1;
	return find_char_bw_it(s, ch, from, i + 1);
}


constexpr int find_char_bw(const std::string_view s, const char ch, const int from)
{
	return find_char_bw_it(s, ch, from, 0);
}


constexpr int skip_string_fw_or_nothing_it(const std::string_view s, const char * const what, const int from, const int i)
{
	if (what[i] == 0) return from + i;
	if (what[i] != s[from + i]) return from;
	return skip_string_fw_or_nothing_it(s, what, from, i + 1);
}


constexpr int skip_string_fw_or_nothing(const std::string_view s, const char * const what, const int from)
{
	return skip_string_fw_or_nothing_it(s, what, from, 0);
}


constexpr int find_string_fw_or_error(const std::string_view s, const char * const what, const int from)
{
	const int pos = find_char_fw(s, what[0], from);
	if (pos == -1) return -1;
	const int matchPos = skip_string_fw_or_nothing(s, what, pos);
	if (matchPos != pos) return pos;
	return find_string_fw_or_error(s, what, pos + 1);
}


constexpr int skip_spaces_fw(const std::string_view s, const int from)
{
	if (s[from] != ' ') return from;
	return skip_spaces_fw(s, from + 1);
}


constexpr int skip_spaces_bw(const std::string_view s, const int from)
{
	if (from == 0) return 0;
	if (s[from - 1] != ' ') return from;
	return skip_spaces_bw(s, from - 1);
}


constexpr int skip_string_bw_or_error_it(const std::string_view s, const char * const what,
		const int whatLength, const int from, const int i)
{
	if (i == whatLength) return from - whatLength;
	if (from == i) return -1;
	if (what[whatLength - i - 1] != s[from - i - 1]) return -1;
	return skip_string_bw_or_error_it(s, what, whatLength, from, i + 1);
}


constexpr int skip_string_bw_or_error(const std::string_view s, const char * const what, const int from)
{
	return skip_string_bw_or_error_it(s, what, Flat::stringLength(what), from, 0);
}


constexpr int skip_paren_stars(const std::string_view s, const int from, const int lastParenPos)
{
	const int parenPos = find_char_fw(s, '(', from);

	if (parenPos == -1) {
		if (lastParenPos == -1) {
			return s.size();
		}

		return lastParenPos;
	}

	const int cdeclPos = skip_string_fw_or_nothing(s, "__cdecl", parenPos);
	const int spacesPos = skip_spaces_fw(s, cdeclPos);
	if (s[spacesPos] != '*') {
		return parenPos;
	}
	const int starPos = spacesPos + 1;

	return skip_paren_stars(s, starPos + 1, parenPos);
}


struct _OpFindInfo {
	bool empty;
	int pos;
};


constexpr _OpFindInfo find_operator(const std::string_view s, const int from)
{
	const int skipEmptyOperatorPos = skip_string_bw_or_error(s, "operator", from);
	if (skipEmptyOperatorPos != -1) {
		return {true, skipEmptyOperatorPos};
	}
	const int spacePos = find_char_bw(s, ' ', from);
	if (spacePos == -1) {
		return {false, -1};
	}
	const int skipSpacesPos = skip_spaces_bw(s, spacePos);
	const int skipNormalOperatorPos = skip_string_bw_or_error(s, "operator", skipSpacesPos);
	if (skipNormalOperatorPos == -1) {
		return {false, -1};
	}
	return {false, skipNormalOperatorPos};
}


constexpr int get_template_begin_pos(const std::string_view s, const int lastTemplateEndPos, const int pos, const int count)
{
	if (pos == 0) {
		if (count != 0) invalid_throw<int>();
		return lastTemplateEndPos;
	}

	const int nextPos = pos - 1;
	const char ch = s[nextPos];

	if (ch == '>') {
		return get_template_begin_pos(s, lastTemplateEndPos, nextPos, count + 1);
	}

	if (ch == '<') {
		if (count == 0) invalid_throw<int>();
		return get_template_begin_pos(s, nextPos, nextPos, count - 1);
	}

	return get_template_begin_pos(s, lastTemplateEndPos, nextPos, count);
}


constexpr std::string_view get_name(const std::string_view s, const int pos, const int last)
{
	const int nameEndPos = get_template_begin_pos(s, pos, pos, 0);
	const int namePos = find_char_bw(s, ' ', nameEndPos);
	if (namePos == -1) {
		return s.substr(0, size_t(last));
	} else {
		return s.substr(namePos + 1, size_t(last - namePos - 1));
	}
}


constexpr std::string_view simplify_func(const std::string_view s)
{
	const int lambdaPos = find_string_fw_or_error(s, "::<", 0);

	const int parenPos = skip_paren_stars(s, 0, -1);

	if (lambdaPos != -1 && lambdaPos < parenPos) {
		return get_name(s, lambdaPos, lambdaPos);
	}

	const int skipSpacesPos = skip_spaces_bw(s, parenPos);

	const auto operatorPos = find_operator(s, skipSpacesPos);

	if (operatorPos.pos != -1) {
		const bool hasEmptyParensAhead = skip_string_fw_or_nothing(s, "()", parenPos) == parenPos + 2;
		if (operatorPos.empty) {
			if (!hasEmptyParensAhead) return invalid_throw<std::string_view>();
		}
		const int suffixSize = hasEmptyParensAhead ? 2 : 0;
		return get_name(s, operatorPos.pos, parenPos + suffixSize);
	}

	return get_name(s, skipSpacesPos, parenPos);
}


constexpr std::string_view extract_func(const std::string_view s, const char * const space)
{
	const std::string_view simplified = simplify_func(s);
	return unspace_func(simplified, space);
}


constexpr std::string_view remove_prefix(const std::string_view s, const std::string_view prefix) noexcept
{
	const auto ss = s.substr(0, prefix.size());
	if (ss != prefix) invalid_throw<int>();
	return s.substr(prefix.size(), s.size() - prefix.size());
}


constexpr std::string_view remove_suffix(const std::string_view s, const std::string_view suffix) noexcept
{
	const auto ss = s.substr(s.size() - suffix.size());
	if (ss != suffix) invalid_throw<int>();
	return s.substr(0, s.size() - suffix.size());
}


constexpr std::string_view remove_last_suffix(const std::string_view s, const std::string_view suffix) noexcept
{
	const auto pos = s.rfind(suffix);
	if (pos == std::string_view::npos) invalid_throw<int>();
	return s.substr(0, pos);
}




class _FunctionLogger : public FunctionLogger
{
public:
	_FunctionLogger(const char * space, const char * name, const char * file, int line,
			const char * function) noexcept :
		FunctionLogger(file, line, function),
		space_(space),
		name_(name),
		threadId_(static_cast<std::uint32_t>(current_thread_id())),
#ifdef FLAT_DEBUG_SIMPLIFY_FUNC_NAME
		func_(extract_func(function, space_))
#else
		func_(unspace_func(function, space_))
#endif
	{
		Log(log_tag(), Log::Priority::Debug, log_output_mode())
				<< Log::format(
					"%+" FLAT_DEBUG_LOG_TAG_INDENT_STRING "s:FNC "
					FLAT_DEBUG_LOG_TIMESTAMP_FORMAT
					FLAT_DEBUG_LOG_THREAD_FORMAT
					FLAT_DEBUG_LOG_OFFSET_FORMAT
					"+ "
					FLAT_DEBUG_LOG_FILE_LINE_FORMAT,
					name_,
					FLAT_DEBUG_LOG_TIMESTAMP_VALUE
					threadId_,
					FLAT_DEBUG_LOG_OFFSET_VALUE
					this->file(), this->line()
					)
				<< Log::SpaceOnce() << func_ << Log::NoSpaceOnce() << FLAT_DEBUG_LOG_FUNC_SUFFIX
				<< "ENTER";

		Offset::instance()->increaseValueForCurrentThread();
	}

	~_FunctionLogger() noexcept
	{
		Offset::instance()->decreaseValueForCurrentThread();

		Log(log_tag(), Log::Priority::Debug, log_output_mode())
				<< Log::format(
					"%+" FLAT_DEBUG_LOG_TAG_INDENT_STRING "s:FNC "
					FLAT_DEBUG_LOG_TIMESTAMP_FORMAT
					FLAT_DEBUG_LOG_THREAD_FORMAT
					FLAT_DEBUG_LOG_OFFSET_FORMAT
					"- "
					FLAT_DEBUG_LOG_FILE_LINE_FORMAT,
					name_,
					FLAT_DEBUG_LOG_TIMESTAMP_VALUE
					threadId_,
					FLAT_DEBUG_LOG_OFFSET_VALUE
					file(), exitLine() == -1 ? line() : exitLine()
					)
				<< Log::SpaceOnce() << func_ << Log::NoSpaceOnce() << FLAT_DEBUG_LOG_FUNC_SUFFIX
				<< "EXIT";
	}

private:
	const char * const space_;
	const char * const name_;
	const size_t threadId_;
	const std::string_view func_;
};




class _NoFunctionLogger
{
public:
	_NoFunctionLogger(const char * space, const char * name, const char * file, int line,
			const char * function)
	{ FLAT_UNUSED(space) FLAT_UNUSED(name) FLAT_UNUSED(file) FLAT_UNUSED(line) FLAT_UNUSED(function) }
	~_NoFunctionLogger() {}
	void setExitLine(int) {}
};




template <bool Enable> struct _CompileTimeFunctionLogger;

template <>
struct _CompileTimeFunctionLogger<true>
{
	typedef _FunctionLogger type;
};

template <>
struct _CompileTimeFunctionLogger<false>
{
	typedef _NoFunctionLogger type;
};




template <typename T, uint64_t V> struct StringMatcher;


constexpr int truncate_pos(uint64_t value, int i = 0)
{
	return i == 8 ? -1 : ((value >> 8*i) & 0xff) == 0 ? i : truncate_pos(value, i + 1);
}


template <bool Enable, typename SM, int Size, int i, uint64_t _Value, uint64_t... _Values>
struct StringImp;


template <typename SM, int Size, int i, uint64_t _Value, uint64_t... _Values>
struct StringImp<false, SM, Size, i, _Value, _Values...>
{
	static constexpr int FullSize = 0;

	typedef void type;
};


template <typename SM, int Size, int i, uint64_t _Value, uint64_t... _Values>
struct StringImp<true, SM, Size, i, _Value, _Values...>
{
	static constexpr int TruncatePosStart = Size == -1 ? -1 : ((Size >> 3) << 3);
	static constexpr int TruncatePosOffset = TruncatePosStart != -1 && i < TruncatePosStart ? -1 :
			Size == -1 ? truncate_pos(_Value) : Size - TruncatePosStart;
	static constexpr bool AtEnd = TruncatePosOffset != -1;

#if defined (__GNUC__) && (__GNUC__ < 4 || (__GNUC__ == 4 && __GNUC_MINOR__ < 8)) || defined(_MSC_VER)
	// Mute warning: left shift count is negative
	// This is the false warning on GCC 4.7, when shift argument is actually positive.
	static constexpr uint64_t Value = !AtEnd ? _Value :
			(_Value & ((uint64_t(1) << (TruncatePosOffset*8 & 0x3F)) - 1));
#else
	static constexpr uint64_t Value = !AtEnd ? _Value :
			(_Value & ((uint64_t(1) << TruncatePosOffset*8) - 1));
#endif

	typedef StringMatcher<SM, Value> ThisSM;

	typedef StringImp<!AtEnd, ThisSM, Size, i + 8, _Values...> NextStringImp;

	static constexpr int FullSize = Size != -1 ? Size : AtEnd ? TruncatePosOffset :
			8 + NextStringImp::FullSize;

	static_assert(!AtEnd || (TruncatePosOffset >= 0 && TruncatePosOffset < 8), "");

	typedef typename std::conditional<AtEnd, ThisSM, typename NextStringImp::type>::type type;
};


template <int Size, uint64_t... _Values>
struct String
{
	typedef StringImp<true, void, Size, 0, _Values...> ThisStringImp;

	static constexpr int FullSize = ThisStringImp::FullSize;

	typedef typename ThisStringImp::type type;
};




template <typename C>
struct DebugExists
{
	static constexpr bool value = false;
};

template <typename C> struct DebugContext;
template <typename C> struct DebugInfo;

struct DebugDump
{
	constexpr DebugDump() = default;
	constexpr DebugDump(const char * space, const char * name, const char * print, bool enable,
			int prefixSize) :
		space(space),
		name(name),
		print(print),
		enable(enable),
		prefixSize(prefixSize)
	{}

	constexpr bool isNull() const { return name == nullptr; }

	const char * space{nullptr};
	const char * name{nullptr};
	const char * print{nullptr};
	bool enable{false};
	int prefixSize{-1};
};




template <uint64_t... Values> struct DebugStructInfo;

constexpr uint64_t _make_debug_template_arg_i(ParsePathIt it, int startPos, uint64_t value, int i)
{
	return i == startPos + 8 ? value :
			_make_debug_template_arg_i(it.atEnd() ? it : it.next(), startPos,
				i < startPos ? 0 :
				value + ((it.atEnd() ? uint64_t(0) : static_cast<uint64_t>(
						static_cast<uint8_t>(it.value()))) << (i - startPos)*8), i + 1);
}

constexpr uint64_t _make_debug_template_arg(ParsePathIt it, int i)
{
	return _make_debug_template_arg_i(it, i*8, 0, 0);
}

#define FLAT_DEBUG_TEMPLATE_ARGS(IT) \
	Flat::Debug::_make_debug_template_arg(IT,  0), \
	Flat::Debug::_make_debug_template_arg(IT,  1), \
	Flat::Debug::_make_debug_template_arg(IT,  2), \
	Flat::Debug::_make_debug_template_arg(IT,  3), \
	Flat::Debug::_make_debug_template_arg(IT,  4), \
	Flat::Debug::_make_debug_template_arg(IT,  5), \
	Flat::Debug::_make_debug_template_arg(IT,  6), \
	Flat::Debug::_make_debug_template_arg(IT,  7), \
	Flat::Debug::_make_debug_template_arg(IT,  8), \
	Flat::Debug::_make_debug_template_arg(IT,  9), \
	Flat::Debug::_make_debug_template_arg(IT, 10), \
	Flat::Debug::_make_debug_template_arg(IT, 11), \
	Flat::Debug::_make_debug_template_arg(IT, 12), \
	Flat::Debug::_make_debug_template_arg(IT, 13), \
	Flat::Debug::_make_debug_template_arg(IT, 14), \
	Flat::Debug::_make_debug_template_arg(IT, 15), \
	Flat::Debug::_make_debug_template_arg(IT, 16), \
	Flat::Debug::_make_debug_template_arg(IT, 17), \
	Flat::Debug::_make_debug_template_arg(IT, 18), \
	Flat::Debug::_make_debug_template_arg(IT, 19), \
	Flat::Debug::_make_debug_template_arg(IT, 20), \
	Flat::Debug::_make_debug_template_arg(IT, 21), \
	Flat::Debug::_make_debug_template_arg(IT, 22), \
	Flat::Debug::_make_debug_template_arg(IT, 23), \
	Flat::Debug::_make_debug_template_arg(IT, 24), \
	Flat::Debug::_make_debug_template_arg(IT, 25), \
	Flat::Debug::_make_debug_template_arg(IT, 26), \
	Flat::Debug::_make_debug_template_arg(IT, 27), \
	Flat::Debug::_make_debug_template_arg(IT, 28), \
	Flat::Debug::_make_debug_template_arg(IT, 29), \
	Flat::Debug::_make_debug_template_arg(IT, 30), \
	Flat::Debug::_make_debug_template_arg(IT, 31)

#define FLAT_DEBUG_TEMPLATE_ARGS_STRING(s) \
	FLAT_DEBUG_TEMPLATE_ARGS(Flat::Debug::ParsePathIt::create(Flat::Debug::kPlatformParseMode, s))

#define FLAT_DEBUG_TEMPLATE_STRUCT(s) \
	Flat::Debug::DebugStructInfo<FLAT_DEBUG_TEMPLATE_ARGS(Flat::Debug::ParsePathIt::create( \
			Flat::Debug::kPlatformParseMode, s))>

#define FLAT_DEBUG_TEMPLATE_STRUCT_IT(IT) \
	Flat::Debug::DebugStructInfo<FLAT_DEBUG_TEMPLATE_ARGS(IT)>




template <typename M>
inline constexpr DebugDump debug_dump_for_match()
{
	using DI = DebugInfo<M>;
	using DC = typename DI::Context;
	constexpr DebugDump dd(DC::space, DC::name, DC::print, DC::enable, DI::prefixSize);
	return dd;
}

#ifdef FLAT_DEBUG_MUTE_UNKNOWN_CONTEXT
template <>
inline constexpr DebugDump debug_dump_for_match<void>()
{
	return DebugDump();
}
#endif




template <bool Exists, int Size, uint64_t... Values> struct BestMatchImp;

template <int Size, uint64_t... Values>
struct BestMatchImp<true, Size, Values...>
{
	typedef typename String<Size, Values...>::type type;
};

template <int Size, uint64_t... Values>
struct BestMatchImp<false, Size, Values...>
{
	typedef typename String<Size, Values...>::type ThisMatch;

	typedef typename BestMatchImp<Size-1, Values...>::type type;
};




template <int Size, uint64_t... Values> struct BestMatch;

template <uint64_t... Values>
struct BestMatch<0, Values...>
{
	typedef void type;
};

template <int Size, uint64_t... Values>
struct BestMatch
{
	typedef String<Size, Values...> ThisString;

	typedef typename ThisString::type ThisMatch;

	static constexpr int FullSize = Size == -1 ? ThisString::FullSize : Size;

	typedef typename std::conditional<DebugExists<ThisMatch>::value, ThisMatch,
			typename BestMatch<FullSize - 1, Values...>::type>::type type;
};




constexpr const char * logPriorityString(Log::Priority p)
{
	return
			p == Log::Priority::Info    ? "INF" :
			p == Log::Priority::Warning ? "WRN" :
			p == Log::Priority::Debug   ? "DBG" :
			p == Log::Priority::Error   ? "ERR" :
			p == Log::Priority::Fatal   ? "FAT" :
			"???";
}




}}




#ifndef __FLAT_BUILD_DIR
#	error "Missing __FLAT_BUILD_DIR macro. It should contain path to the root build directory."
#endif

#define FLAT_DEBUG_FILE_FULL_PATH \
	(Flat::Debug::is_absolute_path(Flat::Debug::kPlatformParseMode, __FILE__) ? \
		__FILE__ : __FLAT_BUILD_DIR "/" __FILE__)

#define FLAT_DEBUG_BEST_MATCH \
		typename Flat::Debug::BestMatch<-1, \
			FLAT_DEBUG_TEMPLATE_ARGS_STRING(FLAT_DEBUG_FILE_FULL_PATH)>::type

#define FLAT_DEBUG_LOG_DEBUG_DUMP \
	Flat::Debug::debug_dump_for_match<FLAT_DEBUG_BEST_MATCH>()

#define FLAT_DEBUG_LOG_SPACE FLAT_DEBUG_LOG_DEBUG_DUMP.space
#define FLAT_DEBUG_LOG_NAME FLAT_DEBUG_LOG_DEBUG_DUMP.name
#define FLAT_DEBUG_LOG_PRINT FLAT_DEBUG_LOG_DEBUG_DUMP.print
#define FLAT_DEBUG_LOG_FILE [] \
		{ constexpr auto s = Flat::Debug::shifted_path(FLAT_DEBUG_FILE_FULL_PATH, \
			FLAT_DEBUG_LOG_DEBUG_DUMP.prefixSize + 1); return s; }()
#define FLAT_DEBUG_LOG_ENABLE FLAT_DEBUG_LOG_DEBUG_DUMP.enable
#define FLAT_DEBUG_LOG_EXCEPTION Flat::Debug::DebugInfo<FLAT_DEBUG_BEST_MATCH>::Context::Error

#ifdef FLAT_DEBUG_SIMPLIFY_FUNC_NAME
#	define FLAT_DEBUG_SIMPLIFIED_FUNC_NAME Flat::Debug::extract_func(f, FLAT_DEBUG_LOG_SPACE)
#else
#	define FLAT_DEBUG_SIMPLIFIED_FUNC_NAME Flat::Debug::unspace_func(f, FLAT_DEBUG_LOG_SPACE)
#endif

#define FLAT_DEBUG_LOG_FUNC \
	[] () -> std::string_view { \
		constexpr std::string_view funcWithoutPrefix = \
				Flat::Debug::remove_prefix(FLAT_DEBUG_FUNC_INFO, FLAT_DEBUG_LAMBDA_PREFIX); \
		constexpr std::string_view f = \
				Flat::Debug::remove_last_suffix(funcWithoutPrefix, FLAT_DEBUG_LAMBDA_SUFFIX); \
		return FLAT_DEBUG_SIMPLIFIED_FUNC_NAME; \
	}()

#define FLAT_DEBUG_LOG_FORMAT(name, priorityString, file, line, func) Flat::Debug::Log::format( \
	"%+" FLAT_DEBUG_LOG_TAG_INDENT_STRING "s:%s " \
	FLAT_DEBUG_LOG_TIMESTAMP_FORMAT \
	FLAT_DEBUG_LOG_THREAD_FORMAT \
	FLAT_DEBUG_LOG_OFFSET_FORMAT \
	FLAT_DEBUG_LOG_FILE_LINE_FORMAT, \
	name, priorityString, \
	FLAT_DEBUG_LOG_TIMESTAMP_VALUE \
	FLAT_DEBUG_LOG_THREAD_VALUE \
	FLAT_DEBUG_LOG_OFFSET_VALUE \
	file, line) \
	<< Flat::Debug::Log::SpaceOnce() << func \
	<< Flat::Debug::Log::NoSpaceOnce() << FLAT_DEBUG_LOG_FUNC_SUFFIX

#define FLAT_DEBUG_LOG_PRIORITY(priority, output) \
	typename Flat::Debug::CompileTimeLog<FLAT_DEBUG_LOG_ENABLE>::type(Flat::Debug::log_tag(), \
			priority, output) \
	<< FLAT_DEBUG_LOG_FORMAT(FLAT_DEBUG_LOG_PRINT, Flat::Debug::logPriorityString(priority), \
		FLAT_DEBUG_LOG_FILE, __LINE__, FLAT_DEBUG_LOG_FUNC) \
	<< Flat::Debug::Log::SetBufferStart()

#define FLAT_DEBUG_LOG_PRIORITY_WITH_EXCEPTION(priority, output) \
	Flat::Debug::Log(Flat::Debug::log_tag(), \
			priority, output) \
	<< FLAT_DEBUG_LOG_FORMAT(FLAT_DEBUG_LOG_PRINT, Flat::Debug::logPriorityString(priority), \
		FLAT_DEBUG_LOG_FILE, __LINE__, FLAT_DEBUG_LOG_FUNC) \
	<< Flat::Debug::Log::SetBufferStart() \
	<< Flat::Debug::ExceptionCallback::Set(Flat::Debug::makeExceptionPointerFromError( \
			Flat::Debug::Error(), \
			Flat::Debug::ExceptionContext{\
				std::string_view(FLAT_DEBUG_LOG_NAME), \
				std::string_view(FLAT_DEBUG_LOG_FILE), __LINE__, \
				std::string_view(FLAT_DEBUG_LOG_FUNC)}))

#define FLAT_INFO    FLAT_DEBUG_LOG_PRIORITY(Flat::Debug::Log::Priority::Info, \
		Flat::Debug::log_output_mode())
#define FLAT_DEBUG   FLAT_DEBUG_LOG_PRIORITY(Flat::Debug::Log::Priority::Debug, \
		Flat::Debug::log_output_mode())
#define FLAT_WARNING FLAT_DEBUG_LOG_PRIORITY(Flat::Debug::Log::Priority::Warning, \
		Flat::Debug::log_output_mode())
#define FLAT_ERROR   FLAT_DEBUG_LOG_PRIORITY(Flat::Debug::Log::Priority::Error, \
		Flat::Debug::log_error_mode())

#define FLAT_CHECK_ERROR(condition) (FLAT_LIKELY(condition) ? \
		Flat::Debug::MaybeLog(Flat::Debug::NoLog()) : \
		Flat::Debug::MaybeLog(FLAT_DEBUG_LOG_PRIORITY_WITH_EXCEPTION( \
				Flat::Debug::Log::Priority::Error, Flat::Debug::log_error_mode())))
#define FLAT_CHECK_WARNING(condition) (FLAT_LIKELY(condition) ? \
		Flat::Debug::MaybeLog(Flat::Debug::NoLog()) : \
		Flat::Debug::MaybeLog(FLAT_DEBUG_LOG_PRIORITY_WITH_EXCEPTION( \
				Flat::Debug::Log::Priority::Warning, Flat::Debug::log_output_mode())))

#define FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION(priority, output, exception) \
	Flat::Debug::Log(Flat::Debug::log_tag(), \
			priority, output) \
	<< FLAT_DEBUG_LOG_FORMAT(FLAT_DEBUG_LOG_PRINT, Flat::Debug::logPriorityString(priority), \
		FLAT_DEBUG_LOG_FILE, __LINE__, FLAT_DEBUG_LOG_FUNC) \
	<< Flat::Debug::Log::SetBufferStart() \
	<< Flat::Debug::ExceptionCallback::Set(Flat::Debug::makeExceptionPointerFromError( \
			exception, \
			Flat::Debug::ExceptionContext{\
				std::string_view(FLAT_DEBUG_LOG_NAME), \
				std::string_view(FLAT_DEBUG_LOG_FILE), __LINE__, \
				std::string_view(FLAT_DEBUG_LOG_FUNC)}))

#define FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION_AND_CONTEXT(priority, output, exception, cerror) \
	Flat::Debug::Log(Flat::Debug::log_tag(), \
			priority, output) \
	<< FLAT_DEBUG_LOG_FORMAT(FLAT_DEBUG_LOG_PRINT, Flat::Debug::logPriorityString(priority), \
		FLAT_DEBUG_LOG_FILE, __LINE__, FLAT_DEBUG_LOG_FUNC) \
	<< Flat::Debug::Log::SetBufferStart() \
	<< Flat::Debug::ExceptionCallback::Set(Flat::Debug::makeExceptionPointerFromError( \
			exception, \
			cerror.context()))

#define FLAT_CHECK(condition) (FLAT_LIKELY(condition) ? \
		Flat::Debug::MaybeLog(Flat::Debug::NoLog()) : \
		Flat::Debug::MaybeLog(FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION( \
				Flat::Debug::Log::Priority::Error, Flat::Debug::log_error_mode(), \
				FLAT_DEBUG_LOG_EXCEPTION())))

#define FLAT_CHECK2(condition, error) (FLAT_LIKELY(condition) ? \
		Flat::Debug::MaybeLog(Flat::Debug::NoLog()) : \
		Flat::Debug::MaybeLog(FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION( \
				Flat::Debug::Log::Priority::Error, Flat::Debug::log_error_mode(), \
				error)))

#define FLAT_THROW_IF_NOT(condition) \
		if (FLAT_UNLIKELY(!(condition))) { throw FLAT_DEBUG_LOG_EXCEPTION() ; }

#define FLAT_FAIL \
		Flat::Debug::ExceptionLog(Flat::Debug::MaybeLog(\
				FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION( \
				Flat::Debug::Log::Priority::Error, Flat::Debug::log_error_mode(), \
				FLAT_DEBUG_LOG_EXCEPTION())))

#define FLAT_FAIL2(error) \
		Flat::Debug::ExceptionLog(Flat::Debug::MaybeLog(\
				FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION( \
				Flat::Debug::Log::Priority::Error, Flat::Debug::log_error_mode(), \
				error)))

#define FLAT_FAIL2C(error, cerror) \
		Flat::Debug::ExceptionLog(Flat::Debug::MaybeLog(\
				FLAT_DEBUG_LOG_PRIORITY_WITH_CUSTOM_EXCEPTION_AND_CONTEXT( \
				Flat::Debug::Log::Priority::Error, Flat::Debug::log_error_mode(), \
				error, cerror)))

#define FLAT_FUNCTION \
	typename Flat::Debug::_CompileTimeFunctionLogger<FLAT_DEBUG_LOG_ENABLE>::type \
		__flat_debug_func_logger(FLAT_DEBUG_LOG_SPACE, FLAT_DEBUG_LOG_PRINT, FLAT_DEBUG_LOG_FILE, __LINE__, \
		FLAT_DEBUG_FUNC_INFO);
#define FLAT_FUNCTION_EXIT __flat_debug_func_logger.setExitLine(__LINE__);

#define FLAT_ASSERT(condition) FLAT_DEBUG_ASSERT_LOG_FATAL(condition, Flat::Debug::log_tag(), \
	Flat::Debug::log_error_mode()) \
	<< FLAT_DEBUG_LOG_FORMAT(FLAT_DEBUG_LOG_PRINT, "ASR", \
		FLAT_DEBUG_LOG_FILE, __LINE__, FLAT_DEBUG_LOG_FUNC) \
	<< Flat::Debug::AssertLog::Condition()

#define FLAT_UNUSED_CHECK_ERROR(condition) FLAT_CHECK_ERROR(condition)
#define FLAT_UNUSED_CHECK_WARNING(condition) FLAT_CHECK_WARNING(condition)
#define FLAT_UNUSED_ASSERT(condition) FLAT_ASSERT(condition)

#define FLAT_FATAL Flat::Debug::FatalLog(Flat::Debug::log_tag(), Flat::Debug::log_error_mode()) \
	<< Flat::Debug::Log::NoOp() \
	<< FLAT_DEBUG_LOG_FORMAT(FLAT_DEBUG_LOG_PRINT, "FAT", \
		FLAT_DEBUG_LOG_FILE, __LINE__, FLAT_DEBUG_LOG_FUNC) \




#define __FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(Space, Name, Print, Prefix, Enable) \
	namespace Flat { namespace Debug { \
	\
	typedef typename String<-1, FLAT_DEBUG_TEMPLATE_ARGS_STRING(Prefix)>::type \
			_DebugContextString##Name; \
	\
	template <> struct DebugExists<_DebugContextString##Name> \
	{ static inline constexpr bool value = true; }; \
	\
	template <> struct DebugContext<_DebugContextString##Name> \
	{ \
		static inline constexpr const char * space = Space; \
		static inline constexpr const char * name = #Name; \
		static inline constexpr const char * print = #Print; \
		static inline constexpr bool enable = Enable; \
		struct Error : public Flat::Debug::Error {}; \
	}; \
	\
	template <> struct DebugInfo<_DebugContextString##Name> \
	{ \
		typedef DebugContext<_DebugContextString##Name> Context; \
		static inline const char * prefix = Prefix; \
		static inline constexpr int prefixSize = Flat::Debug::path_length(Prefix); \
	}; \
	\
	}}

#define FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(Name, Suffix, Prefix) \
	namespace Flat { namespace Debug { \
	\
	typedef typename String<-1, FLAT_DEBUG_TEMPLATE_ARGS_STRING(Prefix)>::type \
			_DebugContextString##Name##Suffix; \
	\
	template <> struct DebugExists<_DebugContextString##Name##Suffix> \
	{ static inline constexpr bool value = true; }; \
	\
	template <> struct DebugInfo<_DebugContextString##Name##Suffix> \
	{ \
		typedef DebugContext<_DebugContextString##Name> Context; \
		static inline const char * prefix = Prefix; \
		static inline constexpr int prefixSize = Flat::Debug::path_length(Prefix); \
	}; \
	\
	}}

#define FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(Name, Prefix, Enable) \
	__FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(nullptr, Name, Name, Prefix, Enable)

#define FLAT_DEBUG_DECLARE_DEBUG_CONTEXT2(Space, Name, Print, Prefix, Enable) \
	__FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(#Space, Name, Print, Prefix, Enable)

#define FLAT_DEBUG_EXCEPTION(Name) \
	Flat::Debug::DebugInfo<Flat::Debug::_DebugContextString##Name>::Context::Error
