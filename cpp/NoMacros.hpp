
#pragma once

#include "Exception.hpp"
#include "Log.hpp"




#define FLAT_INFO    Flat::Debug::NoLog() << Flat::Debug::Log::NoOp()
#define FLAT_DEBUG   Flat::Debug::NoLog() << Flat::Debug::Log::NoOp()
#define FLAT_WARNING Flat::Debug::NoLog() << Flat::Debug::Log::NoOp()
#define FLAT_ERROR   Flat::Debug::NoLog() << Flat::Debug::Log::NoOp()

#define FLAT_CHECK_ERROR(condition)                                   Flat::Debug::NoLog()
#define FLAT_UNUSED_CHECK_ERROR(condition)   FLAT_UNUSED((condition)) Flat::Debug::NoLog()
#define FLAT_CHECK_WARNING(condition)                                 Flat::Debug::NoLog()
#define FLAT_UNUSED_CHECK_WARNING(condition) FLAT_UNUSED((condition)) Flat::Debug::NoLog()

#define FLAT_CHECK(condition) FLAT_UNUSED((condition)) Flat::Debug::NoLog()
#define FLAT_CHECK2(condition, error) FLAT_UNUSED((condition)) FLAT_UNUSED((error)) Flat::Debug::NoLog()

#define FLAT_THROW_IF_NOT(condition) FLAT_UNUSED((condition)) ;

#define FLAT_FAIL Flat::Debug::FatalNoLog()
#define FLAT_FAIL2(error) FLAT_UNUSED(error) Flat::Debug::FatalNoLog()
#define FLAT_FAIL2C(error, cerror) FLAT_UNUSED(error) FLAT_UNUSED(cerror) Flat::Debug::FatalNoLog()

#define FLAT_FUNCTION ;
#define FLAT_FUNCTION_EXIT ;

#define FLAT_UNUSED_ASSERT(condition) FLAT_UNUSED((condition)) Flat::Debug::NoLog()

#ifdef FLAT_FORCE_UNUSED_ASSERTS
#	define FLAT_ASSERT(condition) FLAT_UNUSED_ASSERT(condition)
#else
#	define FLAT_ASSERT(condition) Flat::Debug::NoLog()
#endif

#define FLAT_FATAL Flat::Debug::FatalNoLog()

namespace Flat { namespace Debug { \
	template <typename C> struct NoMacroDebugInfo;
}}

#define FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(Name, Prefix, Enable) \
	namespace Flat { namespace Debug { \
		struct NoMacroDebugType##Name; \
		\
		template <> struct NoMacroDebugInfo<NoMacroDebugType##Name> { \
			struct Error : public Flat::Debug::Error {}; \
		}; \
	}}

#define FLAT_DEBUG_DECLARE_DEBUG_CONTEXT2(Space, Name, Print, Prefix, Enable) \
	FLAT_DEBUG_DECLARE_DEBUG_CONTEXT(Name, Prefix, Enable)

#define FLAT_DEBUG_DECLARE_SECONDARY_DEBUG_CONTEXT(Name, Suffix, Prefix)

#define FLAT_DEBUG_EXCEPTION(Name) Flat::Debug::NoMacroDebugInfo<Flat::Debug::NoMacroDebugType##Name>::Error
