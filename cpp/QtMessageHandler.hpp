
#pragma once

#include <QtMsgHandler>
#include <QString>




namespace Flat {
namespace Debug {




FLAT_DEBUG_QT_EXPORT void qtMessageHandler(QtMsgType type, const QMessageLogContext & context,
		const QString & message) noexcept;

FLAT_DEBUG_QT_EXPORT void setQtContextForPath(const QString & path, const QByteArray & context);




}}
