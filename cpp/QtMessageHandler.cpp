
#include "QtMessageHandler.hpp"

#include <QVector>

#include "Log.hpp"
#include "LogTypesQt.hpp"
#include "Utils.hpp"




namespace Flat {
namespace Debug {




struct QtPathContext
{
	QByteArray path;
	QByteArray context;
};

static QVector<QtPathContext> gPathContexts;




static Log::Priority logPriorityForQtMsgType(QtMsgType type) noexcept
{
	switch (type) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
	case QtInfoMsg:     return Log::Priority::Info;
#endif
	case QtDebugMsg:    return Log::Priority::Debug;
	case QtWarningMsg:  return Log::Priority::Warning;
	case QtCriticalMsg: return Log::Priority::Error;
	case QtFatalMsg:    return Log::Priority::Fatal;
	}

	return Log::Priority::Info;
}


static Log::OutputMode logOutputForQtMsgType(QtMsgType type) noexcept
{
	switch (type) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
	case QtInfoMsg:     return log_output_mode();
#endif
	case QtDebugMsg:    return log_output_mode();
	case QtWarningMsg:  return log_output_mode();
	case QtCriticalMsg: return log_error_mode();
	case QtFatalMsg:    return log_error_mode();
	}

	return log_error_mode();
}


static const char * logLevelForQtMsgType(QtMsgType type) noexcept
{
	switch (type) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 5, 0)
	case QtInfoMsg:     return "INF";
#endif
	case QtDebugMsg:    return "DBG";
	case QtWarningMsg:  return "WRN";
	case QtCriticalMsg: return "ERR";
	case QtFatalMsg:    return "FAT";
	}

	return "???";
}


static const QtPathContext * contextForPath(const char * const path)
{
	if (!path) return nullptr;
	if (gPathContexts.isEmpty()) return nullptr;

	const int pathLength = qstrlen(path);

	for (const QtPathContext & p: gPathContexts) {
		if (p.path.length() >= pathLength) continue;
		if (qstrncmp(p.path.constData(), path, p.path.length()) == 0) return &p;
	}
	return nullptr;
}


void qtMessageHandler(QtMsgType type, const QMessageLogContext & context,
		const QString & message) noexcept
{
	const Log::Priority priority = logPriorityForQtMsgType(type);
	const Log::OutputMode output = logOutputForQtMsgType(type);
	const char * const level = logLevelForQtMsgType(type);

	const QtPathContext * const qtContext = contextForPath(context.file);

#ifdef FLAT_DEBUG_LOG_TRUNCATE_QT_CATEGORY
	char tag[FLAT_DEBUG_LOG_TAG_INDENT];
	snprintf(tag, sizeof(tag), "Qt(%s)", context.category);
#else
	std::string tagString = "Qt(";
	tagString += qtContext ? qtContext->context.constData() : context.category;
	tagString += ")";
	const char * const tag = tagString.c_str();
#endif

	Log(log_tag(), priority, output)
			<< Log::format(
				"%+" FLAT_DEBUG_LOG_TAG_INDENT_STRING "s:%s "
				FLAT_DEBUG_LOG_TIMESTAMP_FORMAT
				FLAT_DEBUG_LOG_THREAD_FORMAT
				FLAT_DEBUG_LOG_OFFSET_FORMAT
				"%s:%d %s -",
				tag,
				level,
				FLAT_DEBUG_LOG_TIMESTAMP_VALUE
				FLAT_DEBUG_LOG_THREAD_VALUE
				FLAT_DEBUG_LOG_OFFSET_VALUE
				context.file ? context.file + (qtContext ? qtContext->path.length() : 0) :
						"?", context.line, context.function ? context.function : "?")
			<< Log::SetBufferStart() << message;
}


void setQtContextForPath(const QString & path, const QByteArray & context)
{
	gPathContexts.append(QtPathContext{path.toUtf8(), context});
}




}}
