
#include "LogTypesStd.hpp"




namespace Flat {
namespace Debug {




template <>
void Log::append<std::string>(const std::string & string) noexcept
{
	print(string.c_str(), int(string.size()));
	maybeSpace();
}

template <>
void Log::append<std::string_view>(const std::string_view & string) noexcept
{
	print(string.data(), int(string.size()));
	maybeSpace();
}

template <>
void Log::append<std::exception>(const std::exception & exception) noexcept
{
	*this << exception.what();
}

template <>
void Log::append<std::runtime_error>(const std::runtime_error & exception) noexcept
{
	*this << exception.what();
}

template <>
void Log::append<std::nullptr_t>(const std::nullptr_t &) noexcept
{
	*this << "<nullptr>";
}




}}
