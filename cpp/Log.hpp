
#pragma once

#include <cstdarg>
#include <cstdio>
#include <cstring>
#include <memory>
#include <string_view>

#include <Flat/Core/Enum.hpp>
#include <Flat/Core/IntegralCast.hpp>
#include <Flat/Core/StringEnum.hpp>
#include <Flat/Core/StringRef.hpp>

#include "NoLog.hpp"




namespace Flat {
namespace Debug {




class FLAT_DEBUG_EXPORT Log
{
public:
	// make internal buffer size to match standard Android logcat line length
	static const int kBufferSize = 1024;
	static const char kNullString[];
	static const int kNullStringSize = 6;

	enum class OutputMode
	{
		Null   = 0x0,
		StdOut = 0x1,
		StdErr = 0x2,
		Logcat = 0x4,
	};

	enum class Priority
	{
		Null,
		Info,
		Warning,
		Debug,
		Error,
		Fatal,
	};

	class NoOp {};
	class Space {};
	class SpaceOnce {};
	class NoSpace {};
	class NoSpaceOnce {};
	class SetBufferStart {};
	class Flush {};

	typedef const void * Pointer;

	class FLAT_DEBUG_EXPORT State
	{
	public:
		State(Log & log) noexcept;
		~State();

	private:
		Log & log_;
		bool putSpace_;
	};

	template <typename T>
	class Hex
	{
	public:
		Hex(const T & value, bool aligned) noexcept : value_(value), aligned_(aligned) {}

		const T & value() const noexcept { return value_; }
		bool aligned() const noexcept { return aligned_; }

	private:
		const T value_;
		const bool aligned_;
	};

	template <typename T>
	class Oct
	{
	public:
		Oct(const T & value, bool aligned) noexcept : value_(value), aligned_(aligned) {}

		const T & value() const noexcept { return value_; }
		bool aligned() const noexcept { return aligned_; }

	private:
		const T value_;
		const bool aligned_;
	};

	struct Raw {
		Raw(const std::string_view & data) noexcept : data(data) {}

		const std::string_view data;
	};

	class Memory
	{
	public:
		Memory(const void * data, int size) noexcept : data_(data), size_(size) {}

		const void * data() const noexcept { return data_; }
		int size() const noexcept { return size_; }

	private:
		const void * const data_;
		const int size_;
	};

	template <typename T>
	class Wrapped
	{
	public:
		Wrapped(const T & value, const char * token) noexcept : value_(value), token_(token) {}

		const T & value() const noexcept { return value_; }
		const char * token() const noexcept { return token_; }

	private:
		const T & value_;
		const char * const token_;
	};

	template <typename K, typename V>
	class KeyValue
	{
	public:
		KeyValue(const K & key, const V & value) noexcept :
			key_(key), value_(value)
		{}

		const K & key() const noexcept { return key_; }
		const V & value() const noexcept { return value_; }

	private:
		const K & key_;
		const V & value_;
	};

	class Format
	{
	public:
		Format(const char * format, va_list args) noexcept
		{
			size_ = std::vsnprintf(buffer_, sizeof(buffer_) - 1, format, args);
			va_end(args);
		}

		const char * buffer() const noexcept { return buffer_; }
		int size() const noexcept { return size_; }

	private:
		char buffer_[1024];
		int size_;
	};

	class AutoFlush
	{
	public:
		AutoFlush(bool set) noexcept : isSet_(set) {}
		bool isSet() const noexcept { return isSet_; }

	private:
		const bool isSet_;
	};

	class OutputCallback
	{
	public:
		OutputCallback() = default;
		virtual ~OutputCallback() {}
		virtual void write(Log & log) noexcept = 0;
	};

	class FlushCallback
	{
	public:
		FlushCallback() = default;
		virtual ~FlushCallback() {}
		virtual void flushed(Log & log) = 0;
	};

	class SetOutputCallback
	{
	public:
		SetOutputCallback(const std::shared_ptr<OutputCallback> & callback) noexcept :
			callback(callback) {}

		const std::shared_ptr<OutputCallback> callback;
	};

	class SetFlushCallback
	{
	public:
		SetFlushCallback(const std::shared_ptr<FlushCallback> callback) noexcept :
			callback(callback) {}

		const std::shared_ptr<FlushCallback> callback;
	};

	class Append
	{
	public:
		Append(Log & log) noexcept : log_(log) {}
		Log & log() noexcept { return log_; }

	private:
		Log & log_;
	};

	template <typename T>
	struct _TypeDoesNotThrowsException
	{
		static constexpr bool value = true;
	};

	template <typename T>
	static Hex<T> hex(const T & value) noexcept { return Hex<T>(value, false); }

	template <typename T>
	static Hex<T> alignedHex(const T & value) noexcept { return Hex<T>(value, true); }

	template <typename T>
	static Oct<T> oct(const T & value) noexcept { return Oct<T>(value, false); }

	template <typename T>
	static Oct<T> alignedOct(const T & value) noexcept { return Oct<T>(value, true); }

	template <typename T>
	static Hex<intptr_t> address(const T * value) noexcept
	{ return Hex<intptr_t>(pointer_to_int_cast<intptr_t, T>(value), false); }

	static Raw raw(const std::string_view & data) noexcept { return Raw(data); }

	template <typename T>
	static Memory memory(const T & value) noexcept { return Memory(&value, sizeof(T)); }

	static Memory memory(const void * data, int size) noexcept { return Memory(data, size); }

	template <typename T>
	static Wrapped<T> wrapped(const T & value, const char * token) noexcept
	{ return Wrapped<T>(value, token); }

	template <typename T>
	static Wrapped<T> squared(const T & value) noexcept { return Wrapped<T>(value, "["); }

	template <typename T>
	static Wrapped<T> braced(const T & value) noexcept { return Wrapped<T>(value, "("); }

	template <typename T>
	static Wrapped<T> curved(const T & value) noexcept { return Wrapped<T>(value, "{"); }

	template <typename T>
	static Wrapped<T> triangled(const T & value) noexcept { return Wrapped<T>(value, "<"); }

	template <typename T>
	static Wrapped<T> quoted(const T & value) noexcept { return Wrapped<T>(value, "\""); }

	template <typename K, typename V>
	static KeyValue<K, V> keyValue(const K & key, const V & value, bool autoFlush = false) noexcept
	{ return KeyValue<K, V>(key, value, autoFlush); }

	static Format format(const char * format, ...) noexcept
	{
		va_list args;
		va_start(args, format);
		return Format(format, args);
	}

	static AutoFlush autoFlush(bool set = true) noexcept { return AutoFlush(set); }

	static const char * defaultTag() noexcept
	{
#ifdef FLAT_DEBUG_LOG_DEFAULT_TAG
		return FLAT_DEBUG_LOG_DEFAULT_TAG;
#else
		return "Flat::Debug::Log";
#endif
	}

	static constexpr Enum<OutputMode> defaultOutputMode() noexcept
	{
#ifdef FLAT_DEBUG_LOG_DEFAULT_OUTPUT_MODE
		return FLAT_DEBUG_LOG_DEFAULT_OUTPUT_MODE;
#else
		return OutputMode::Null;
#endif
	}

	static OutputCallback * globalOutputCallback() noexcept;
	static void setGlobalOutputCallback(OutputCallback * callback) noexcept;

	static FlushCallback * globalFlushCallback() noexcept;
	static void setGlobalFlushCallback(FlushCallback * callback) noexcept;

	Log() noexcept;
	Log(const char * tag, Priority priority, Enum<OutputMode> outputMode) noexcept;
	Log(Log && log) noexcept;
	~Log() noexcept(false);

	Priority priority() const noexcept { return priority_; }
	Enum<OutputMode> outputMode() const noexcept { return outputMode_; }

	void maybeSpace() noexcept;

	const char * buffer() const noexcept;
	int bufferSize() const noexcept;

	// helpers
	template <typename T> void append(const T & value) noexcept(_TypeDoesNotThrowsException<T>::value);
	template <typename T> void appendPointer(const T * pointer) noexcept;
	template <typename T> void appendPointer(T * pointer) noexcept;
	template <typename T> void appendHex(const Hex<T> & hex) noexcept;
	template <typename T> void appendOct(const Oct<T> & oct) noexcept;
	void appendRaw(const Raw & raw) noexcept;
	void appendMemory(const Memory & memory) noexcept;
	template <typename T> void appendWrapped(const Wrapped<T> & wrapped) noexcept;
	void appendFormat(const Format & format) noexcept;

	// operator<<() overloads
	template <typename T> Log & operator<<(const T & value) noexcept;
	template <typename T> Log & operator<<(const T * pointer) noexcept;
	template <typename T> Log & operator<<(T * pointer) noexcept;
	template <typename T> Log & operator<<(const Hex<T> & hex) noexcept;
	template <typename T> Log & operator<<(const Oct<T> & oct) noexcept;
	Log & operator<<(const Raw & raw) noexcept;
	Log & operator<<(const Memory & memory) noexcept;
	template <typename T> Log & operator<<(const Wrapped<T> & wrapped) noexcept;
	Log & operator<<(const Format & format) noexcept;

	// C-strings
	void appendString(const char * value) noexcept;
	void append(const char * value) noexcept;
	Log & operator<<(const char * value) noexcept;
	void append(char * value) noexcept;
	Log & operator<<(char * value) noexcept;
	template <int S> void append(char const (&value)[S]) noexcept;
	template <int S> Log & operator<<(char const (&value)[S]) noexcept;

	// integers
	Log & operator<<(int value) noexcept;

	// append() overloads
	void append(const Format & format) noexcept;

	void print(const char * text, int size) noexcept;
	void print(const char * text) noexcept;
	void printf(const char * format, ...) noexcept;
	void vprintf(const char * format, va_list args) noexcept;

	void flush();

private:
	template <int S, typename T>
	class HexPrinter
	{
	public:
		static void sprint(char * buf, const Hex<T> & hex) noexcept;
	};

private:
	void _init() noexcept;
	void _flush(bool final);
	void _printDouble(double value) noexcept;
	void _printHexString(const char * s) noexcept;

private:
	const char * tag_ = "";
	Priority priority_ = Priority::Null;
	Enum<OutputMode> outputMode_ = defaultOutputMode();

	int bufferStart_ = 0;
	char buffer_[kBufferSize+1]; // +1 for trailing '\n' in case writing to stdout
	int size_ = 0;
	bool dirty_ = false;
	bool putSpace_ = true;
	bool autoFlush_ = true;

	std::shared_ptr<OutputCallback> outputCallback_;
	std::shared_ptr<FlushCallback> flushCallback_;

	static OutputCallback * gOutputCallback;
	static FlushCallback * gFlushCallback;

	friend class State;
};




FLAT_CORE_DEFINE_ENUM_OPS(Log::OutputMode)



template <typename T>
void operator<<(Log::Append & append, const T & value) noexcept;




class MaybeLog
{
public:
	MaybeLog() noexcept;
	MaybeLog(Log & log) noexcept;
	MaybeLog(const NoLog &) noexcept;
	MaybeLog(MaybeLog && maybeLog) noexcept;

	template <typename T>
	MaybeLog & operator<<(const T & value) noexcept;

	void flush();

private:
	Log log_;
};




class ExceptionLog
{
public:
	ExceptionLog(MaybeLog && log) noexcept : log_(std::move(log)) {}

	[[noreturn]] ~ExceptionLog() { log_.flush(); std::abort(); }

	template <typename T>
	ExceptionLog & operator<<(const T & value) noexcept { log_ << value; return *this; }

private:
	MaybeLog log_;
};




template <bool Enable> struct CompileTimeLog;

template <>
struct CompileTimeLog<true>
{
	typedef Log type;
};

template <>
struct CompileTimeLog<false>
{
	typedef NoLog type;
};




inline void Log::_init() noexcept
{
	buffer_[0] = 0;
}


inline Log::Log() noexcept
{}


inline Log::Log(const char * const tag, const Priority priority,
		const Enum<OutputMode> outputMode) noexcept :
	tag_(tag),
	priority_(priority),
	outputMode_(outputMode)
{
	_init();
}


inline const char * Log::buffer() const noexcept
{ return buffer_; }

inline int Log::bufferSize() const noexcept
{ return size_; }


inline void Log::maybeSpace() noexcept
{
	if (putSpace_) {
		char sp = ' ';
		print(&sp, 1);
	}
}


template <>
struct Log::_TypeDoesNotThrowsException<Log::Flush>
{
	static constexpr bool value = false;
};


template <typename T>
inline void Log::append(const T & value) noexcept(_TypeDoesNotThrowsException<T>::value)
{
	Append a(*this);
	a << value;
}


template <>
inline void Log::append<Log::NoOp>(const NoOp &) noexcept
{}


inline void Log::appendString(const char * const value) noexcept
{
	if (!value) {
		print(kNullString, kNullStringSize);
	} else {
		print(value, int(std::strlen(value)));
	}
	maybeSpace();
}


inline void Log::append(const char * const value) noexcept
{
	appendString(value);
}


inline Log & Log::operator<<(const char * const value) noexcept
{
	appendString(value);
	return *this;
}


inline void Log::append(char * const value) noexcept
{
	appendString(value);
}


inline Log & Log::operator<<(char * const value) noexcept
{
	appendString(value);
	return *this;
}


template <int S>
inline void Log::append(char const (&value)[S]) noexcept
{
	print(value, Flat::strnlen(value, S-1));
	maybeSpace();
}


template <int S>
inline Log & Log::operator<<(char const (&value)[S]) noexcept
{
	print(value, Flat::strnlen(value, S-1));
	maybeSpace();
	return *this;
}


template <typename T>
inline void Log::appendPointer(const T * const value) noexcept
{
	char buf[17];
	std::sprintf(buf, "%p", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <typename T>
inline void Log::appendPointer(T * const value) noexcept
{
	char buf[17];
	std::sprintf(buf, "%p", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <>
inline void Log::append<char>(const char & c) noexcept
{
	print(&c, 1);
	maybeSpace();
}


template <>
inline void Log::append<signed char>(const signed char & c) noexcept
{
	print(reinterpret_cast<const char*>(&c), 1);
	maybeSpace();
}


template <>
inline void Log::append<char32_t>(const char32_t & c) noexcept
{
	char buf[16];
	const auto length = std::sprintf(buf, "%08x", uint32_t(c));
	print(buf, length);
	maybeSpace();
}


template <>
inline void Log::append<bool>(const bool & value) noexcept
{
	if (value) {
		print("true", 4);
	} else {
		print("false", 5);
	}
	maybeSpace();
}


template <>
inline void Log::append<int>(const int & value) noexcept
{
	char buf[16];
	std::sprintf(buf, "%d", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <>
inline void Log::append<unsigned int>(const unsigned int & value) noexcept
{
	char buf[16];
	std::sprintf(buf, "%u", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <>
inline void Log::append<unsigned char>(const unsigned char & c) noexcept
{
	append<unsigned int>(static_cast<unsigned int>(c));
}


template <>
inline void Log::append<short int>(const short int & value) noexcept
{
	append<int>(static_cast<int>(value));
}


template <>
inline void Log::append<unsigned short int>(const unsigned short int & value) noexcept
{
	append<unsigned int>(static_cast<unsigned int>(value));
}


template <>
inline void Log::append<long>(const long & value) noexcept
{
	char buf[32];
	std::sprintf(buf, "%ld", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <>
inline void Log::append<unsigned long>(const unsigned long & value) noexcept
{
	char buf[32];
	std::sprintf(buf, "%lu", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <>
inline void Log::append<long long>(const long long & value) noexcept
{
	char buf[32];
	std::sprintf(buf, "%lld", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


template <>
inline void Log::append<unsigned long long>(const unsigned long long & value) noexcept
{
	char buf[32];
	std::sprintf(buf, "%llu", value);
	print(buf, int(std::strlen(buf)));
	maybeSpace();
}


inline void Log::_printDouble(const double value) noexcept
{
	char buf[64];
	std::sprintf(buf, "%g", value);
	if (!::strchr(buf, '.') && !::strchr(buf, 'e') && !::strchr(buf, 'E')) {
		std::strncat(buf, ".0", sizeof(buf) - 1);
	}
	print(buf, int(std::strlen(buf)));
}


template <>
inline void Log::append<float>(const float & value) noexcept
{
	_printDouble(double(value));
	maybeSpace();
}


template <>
inline void Log::append<double>(const double & value) noexcept
{
	_printDouble(value);
	maybeSpace();
}


template <typename T>
class Log::HexPrinter<1,T>
{
public:
	static void sprint(char * buf, const Hex<T> & hex) noexcept
	{
		if (hex.aligned()) {
			std::sprintf(buf, "0x%08x", static_cast<uint32_t>(hex.value()));
		} else {
			std::sprintf(buf, "0x%x", static_cast<uint32_t>(hex.value()));
		}
	}
};


template <typename T>
class Log::HexPrinter<2,T>
{
public:
	static void sprint(char * buf, const Hex<T> & hex) noexcept
	{
		if (hex.aligned()) {
			std::sprintf(buf, "0x%08x", static_cast<uint32_t>(hex.value()));
		} else {
			std::sprintf(buf, "0x%x", static_cast<uint32_t>(hex.value()));
		}
	}
};


template <typename T>
class Log::HexPrinter<4,T>
{
public:
	static void sprint(char * buf, const Hex<T> & hex) noexcept
	{
		if (hex.aligned()) {
			std::sprintf(buf, "0x%08x", static_cast<uint32_t>(hex.value()));
		} else {
			std::sprintf(buf, "0x%x", static_cast<uint32_t>(hex.value()));
		}
	}
};


template <typename T>
class Log::HexPrinter<8,T>
{
public:
	static void sprint(char * buf, const Hex<T> & hex) noexcept
	{
		if ( hex.aligned() ) {
			std::sprintf(buf, "0x%016llx", static_cast<unsigned long long int>(hex.value()));
		} else {
			std::sprintf(buf, "0x%llx", static_cast<unsigned long long int>(hex.value()));
		}
	}
};


template <typename T>
inline void Log::appendHex(const Hex<T> & value) noexcept
{
	char buf[64];
	HexPrinter<sizeof(T), T>::sprint(buf, value);
	print(buf);
	maybeSpace();
}


template <>
inline void Log::appendHex(const Hex<const char *> & value) noexcept
{
	_printHexString(value.value());
}


template <>
inline void Log::appendHex(const Hex<char *> & value) noexcept
{
	_printHexString(value.value());
}


template <typename T>
inline void Log::appendOct(const Oct<T> & oct) noexcept
{
	print("0", 1);
	bool force = oct.aligned();
	for (int i = sizeof(T)*8/3 - 1; i >= 0; --i) {
		const char ch = '0' + ((oct.value() >> (i*3)) & 7);
		if (ch != '0') force = true;
		if (force) print(&ch, 1);
	}
	maybeSpace();
}


inline char hexChar(uint8_t ch) noexcept
{
	FLAT_PLAIN_ASSERT(ch < 16)
	return ch < 10 ? char('0' + ch) : char('A' + (ch - 10));
}


inline void Log::appendRaw(const Raw & raw) noexcept
{
	print(raw.data.data(), raw.data.size());
}


inline void Log::appendMemory(const Memory & memory) noexcept
{
	printf("(Memory: size = %d, data = ", memory.size());

	for (int i = 0; i < memory.size(); ++i) {
		const uint8_t ch = static_cast<const uint8_t*>(memory.data())[i];
		const char lo = hexChar(ch & 0xf);
		const char hi = hexChar(ch >> 4);
		print(&hi, 1);
		print(&lo, 1);

		if (i < memory.size() - 1) {
			print(" ", 1);
		}
	}

	print(")");
	maybeSpace();
}


template <typename T>
inline void Log::appendWrapped(const Wrapped<T> & wrapped) noexcept
{
	const int tokenLength = wrapped.token() ? int(std::strlen(wrapped.token())) : 0;

	// opening token
	if (tokenLength > 0) {
		print(wrapped.token(), tokenLength);
	}

	*this << wrapped.value();
	*this << NoSpaceOnce();

	// closing token, backward direction
	if (tokenLength > 0) {
		for (int i = tokenLength - 1; i >= 0; i--) {
			char ch;
			switch ( wrapped.token()[i] ) {
			case '[': ch = ']'; break;
			case '(': ch = ')'; break;
			case '{': ch = '}'; break;
			case '<': ch = '>'; break;
			default: ch = wrapped.token()[i];
			}
			print(&ch, 1);
		}
	}

	maybeSpace();
}


inline void Log::appendFormat(const Format & format) noexcept
{
	print(format.buffer(), format.size());
	maybeSpace();
}


template <typename T>
inline Log & Log::operator<<(const T & value) noexcept
{ append<T>(value); return *this; }

template <typename T>
inline Log & Log::operator<<(const T * pointer) noexcept
{ appendPointer<T>(pointer); return *this; }

template <typename T>
inline Log & Log::operator<<(T * pointer) noexcept
{ appendPointer<T>(pointer); return *this; }

template <typename T>
inline Log & Log::operator<<(const Hex<T> & hex) noexcept
{ appendHex<T>(hex); return *this; }

template <typename T>
inline Log & Log::operator<<(const Oct<T> & oct) noexcept
{ appendOct<T>(oct); return *this; }

inline Log & Log::operator<<(const Raw & raw) noexcept
{ appendRaw(raw); return *this; }

inline Log & Log::operator<<(const Memory & memory) noexcept
{ appendMemory(memory); return *this; }

template <typename T>
inline Log & Log::operator<<(const Wrapped<T> & wrapped) noexcept
{ appendWrapped<T>(wrapped); return *this; }

inline Log & Log::operator<<(const Format & format) noexcept
{ appendFormat(format); return *this; }


template <>
inline void Log::append<Log::Memory>(const Log::Memory & memory) noexcept
{ appendMemory(memory); }


template <typename K, typename V>
inline void operator<<(Log::Append & append, const Log::KeyValue<K, V> & kv) noexcept
{ append.log() << kv.key() << Log::NoSpaceOnce() << '=' << Log::NoSpaceOnce() << kv.value(); }


inline Log & Log::operator<<(const int value) noexcept
{
	append<int>(value);
	return *this;
}


inline void Log::append(const Format & format) noexcept
{ return appendFormat(format); }




// types

template <typename String>
inline void operator<<(Log::Append & append, const Flat::StringRefBase<String> & string) noexcept
{
	if (string.isNull()) {
		append.log().print(Log::kNullString, Log::kNullStringSize);
	} else {
		append.log().print(string.string(), string.size());
	}
	append.log().maybeSpace();
}


template <typename E>
inline void operator<<(Log::Append & append, const Flat::StringEnum<E> & value) noexcept
{
	if (value == Flat::StringEnumInfo<E>::null) {
		append.log().print(Log::kNullString, Log::kNullStringSize);
	} else {
		const char * const stringValue = value.toString();
		if (!stringValue) {
			append.log().printf("(Invalid %s: %d)", Flat::StringEnumTraits<E>::name,
					static_cast<unsigned int>(value.type()));
		} else {
			append.log().print(stringValue);
		}
	}
	append.log().maybeSpace();
}


template <typename E>
inline void operator<<(Log::Append & append, const Flat::Enum<E> & value) noexcept
{
	if (StringEnumTraits<E>::flags) {
		bool first = true;
		for (const auto e : value.flags()) {
			const auto es = StringEnum<E>(e);
			if (!first) {
				append.log() << "|";
			}
			first = false;
			append.log() << static_cast<E>(e);
		}
	} else {
		append.log() << static_cast<E>(value);
	}
	append.log().maybeSpace();
}




// forward declatations
template <> FLAT_DEBUG_EXPORT void Log::append<Log::Space>(const Space &) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::SpaceOnce>(const SpaceOnce &) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::NoSpace>(const NoSpace &) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::NoSpaceOnce>(const NoSpaceOnce &) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::SetBufferStart>(
		const SetBufferStart &) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::Flush>(const Flush &);
template <> FLAT_DEBUG_EXPORT void Log::append<Log::AutoFlush>(
		const AutoFlush & autoFlush) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::SetOutputCallback>(
		const SetOutputCallback & setOutputCallback) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<Log::SetFlushCallback>(
		const SetFlushCallback & setFlushCallback) noexcept;




inline MaybeLog::MaybeLog() noexcept
{}

inline MaybeLog::MaybeLog(Log & log) noexcept :
	log_(std::move(log))
{}

inline MaybeLog::MaybeLog(const NoLog &) noexcept
{}

inline MaybeLog::MaybeLog(MaybeLog && maybeLog) noexcept :
	log_(std::move(maybeLog.log_))
{}

template <typename T>
inline MaybeLog & MaybeLog::operator<<(const T & value) noexcept
{
	if (FLAT_LIKELY(log_.priority() != Log::Priority::Null)) {
		log_ << value;
	}
	return *this;
}

inline void MaybeLog::flush()
{
	if (FLAT_LIKELY(log_.priority() != Log::Priority::Null)) {
		log_.flush();
	}
}




}}
