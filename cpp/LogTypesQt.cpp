
#include "LogTypesQt.hpp"

#include <QDir>
#include <QJsonValue>
#include <QModelIndex>
#include <QObject>
#include <QRect>
#include <QStringList>
#include <QUrl>

#ifdef QT_NETWORK_LIB
#include <QHostAddress>
#endif

#ifdef QT_GUI_LIB
#include <QColor>
#include <QScreen>
#include <QVector3D>
#endif

#ifdef QT_QML_LIB
#include <QQmlError>
#endif




namespace Flat {
namespace Debug {




template <>
void Log::append<QByteArray>(const QByteArray & byteArray) noexcept
{
	print(byteArray.constData(), byteArray.size());
	maybeSpace();
}


template <>
void Log::append<QString>(const QString & string) noexcept
{
	print(qPrintable(string));
	maybeSpace();
}


template <>
void Log::append<QDataStream::Status>(const QDataStream::Status & status) noexcept
{
	switch (status) {
	case QDataStream::Ok:              appendString("Ok"); return;
	case QDataStream::ReadPastEnd:     appendString("ReadPastEnd"); return;
	case QDataStream::ReadCorruptData: appendString("ReadCorruptData"); return;
	case QDataStream::WriteFailed:     appendString("WriteFailed"); return;
	}

	*this << "(Invalid status:" << int(status) << NoSpaceOnce() << ")";
}


template <>
void Log::append<QEvent::Type>(const QEvent::Type & type) noexcept
{
#define FLAT_DEBUG_LOG_QEVENT_TYPE(x) \
	case QEvent::x: appendString( #x ); return;

	switch (type) {
	FLAT_DEBUG_LOG_QEVENT_TYPE(None)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Timer)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MouseButtonPress)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MouseButtonRelease)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MouseButtonDblClick)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MouseMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(KeyPress)
	FLAT_DEBUG_LOG_QEVENT_TYPE(KeyRelease)
	FLAT_DEBUG_LOG_QEVENT_TYPE(FocusIn)
	FLAT_DEBUG_LOG_QEVENT_TYPE(FocusOut)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Enter)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Leave)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Paint)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Move)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Resize)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Create)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Destroy)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Show)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Hide)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Close)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Quit)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ParentChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ParentAboutToChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ThreadChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowActivate)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowDeactivate)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ShowToParent)
	FLAT_DEBUG_LOG_QEVENT_TYPE(HideToParent)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Wheel)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowTitleChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowIconChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationWindowIconChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationFontChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationLayoutDirectionChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationPaletteChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(PaletteChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Clipboard)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Speech)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MetaCall)
	FLAT_DEBUG_LOG_QEVENT_TYPE(SockAct)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WinEventAct)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DeferredDelete)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DragEnter)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DragMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DragLeave)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Drop)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DragResponse)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ChildAdded)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ChildPolished)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ChildRemoved)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ShowWindowRequest)
	FLAT_DEBUG_LOG_QEVENT_TYPE(PolishRequest)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Polish)
	FLAT_DEBUG_LOG_QEVENT_TYPE(LayoutRequest)
	FLAT_DEBUG_LOG_QEVENT_TYPE(UpdateRequest)
	FLAT_DEBUG_LOG_QEVENT_TYPE(UpdateLater)
	FLAT_DEBUG_LOG_QEVENT_TYPE(EmbeddingControl)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ActivateControl)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DeactivateControl)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ContextMenu)
	FLAT_DEBUG_LOG_QEVENT_TYPE(InputMethod)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TabletMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(LocaleChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(LanguageChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(LayoutDirectionChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Style)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TabletPress)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TabletRelease)
	FLAT_DEBUG_LOG_QEVENT_TYPE(OkRequest)
	FLAT_DEBUG_LOG_QEVENT_TYPE(HelpRequest)
	FLAT_DEBUG_LOG_QEVENT_TYPE(IconDrag)
	FLAT_DEBUG_LOG_QEVENT_TYPE(FontChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(EnabledChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ActivationChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(StyleChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(IconTextChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ModifiedChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MouseTrackingChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowBlocked)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowUnblocked)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowStateChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ToolTip)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WhatsThis)
	FLAT_DEBUG_LOG_QEVENT_TYPE(StatusTip)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ActionChanged)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ActionAdded)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ActionRemoved)
	FLAT_DEBUG_LOG_QEVENT_TYPE(FileOpen)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Shortcut)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ShortcutOverride)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WhatsThisClicked)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ToolBarChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationActivate)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationDeactivate)
	FLAT_DEBUG_LOG_QEVENT_TYPE(QueryWhatsThis)
	FLAT_DEBUG_LOG_QEVENT_TYPE(EnterWhatsThisMode)
	FLAT_DEBUG_LOG_QEVENT_TYPE(LeaveWhatsThisMode)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ZOrderChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(HoverEnter)
	FLAT_DEBUG_LOG_QEVENT_TYPE(HoverLeave)
	FLAT_DEBUG_LOG_QEVENT_TYPE(HoverMove)
#ifdef QT_KEYPAD_NAVIGATION
	FLAT_DEBUG_LOG_QEVENT_TYPE(EnterEditFocus)
	FLAT_DEBUG_LOG_QEVENT_TYPE(LeaveEditFocus)
#endif
	FLAT_DEBUG_LOG_QEVENT_TYPE(AcceptDropsChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ZeroTimerEvent)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneMouseMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneMousePress)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneMouseRelease)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneMouseDoubleClick)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneContextMenu)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneHoverEnter)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneHoverMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneHoverLeave)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneHelp)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneDragEnter)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneDragMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneDragLeave)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneDrop)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneWheel)
	FLAT_DEBUG_LOG_QEVENT_TYPE(KeyboardLayoutChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(DynamicPropertyChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TabletEnterProximity)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TabletLeaveProximity)
	FLAT_DEBUG_LOG_QEVENT_TYPE(NonClientAreaMouseMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(NonClientAreaMouseButtonPress)
	FLAT_DEBUG_LOG_QEVENT_TYPE(NonClientAreaMouseButtonRelease)
	FLAT_DEBUG_LOG_QEVENT_TYPE(NonClientAreaMouseButtonDblClick)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MacSizeChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ContentsRectChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MacGLWindowChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(FutureCallOut)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneResize)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GraphicsSceneMove)
	FLAT_DEBUG_LOG_QEVENT_TYPE(CursorChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ToolTipChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(NetworkReplyUpdated)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GrabMouse)
	FLAT_DEBUG_LOG_QEVENT_TYPE(UngrabMouse)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GrabKeyboard)
	FLAT_DEBUG_LOG_QEVENT_TYPE(UngrabKeyboard)
	FLAT_DEBUG_LOG_QEVENT_TYPE(MacGLClearDrawable)
	FLAT_DEBUG_LOG_QEVENT_TYPE(StateMachineSignal)
	FLAT_DEBUG_LOG_QEVENT_TYPE(StateMachineWrapped)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TouchBegin)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TouchUpdate)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TouchEnd)
#ifndef QT_NO_GESTURES
	FLAT_DEBUG_LOG_QEVENT_TYPE(NativeGesture)
#endif
	FLAT_DEBUG_LOG_QEVENT_TYPE(RequestSoftwareInputPanel)
	FLAT_DEBUG_LOG_QEVENT_TYPE(CloseSoftwareInputPanel)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WinIdChange)
#ifndef QT_NO_GESTURES
	FLAT_DEBUG_LOG_QEVENT_TYPE(Gesture)
	FLAT_DEBUG_LOG_QEVENT_TYPE(GestureOverride)
#endif
	FLAT_DEBUG_LOG_QEVENT_TYPE(FocusAboutToChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ReadOnlyChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ScrollPrepare)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Scroll)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Expose)
	FLAT_DEBUG_LOG_QEVENT_TYPE(InputMethodQuery)
	FLAT_DEBUG_LOG_QEVENT_TYPE(OrientationChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TouchCancel)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ThemeChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(SockClose)
	FLAT_DEBUG_LOG_QEVENT_TYPE(PlatformPanel)
	FLAT_DEBUG_LOG_QEVENT_TYPE(StyleAnimationUpdate)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ApplicationStateChange)
	FLAT_DEBUG_LOG_QEVENT_TYPE(WindowChangeInternal)
	FLAT_DEBUG_LOG_QEVENT_TYPE(ScreenChangeInternal)
	FLAT_DEBUG_LOG_QEVENT_TYPE(PlatformSurface)
	FLAT_DEBUG_LOG_QEVENT_TYPE(Pointer)
	FLAT_DEBUG_LOG_QEVENT_TYPE(TabletTrackingChange)

	case QEvent::User:
	case QEvent::MaxUser:
		break;
	}

#undef FLAT_DEBUG_LOG_QEVENT_TYPE

	*this << "(Unknown QEvent::Type:" << int(type) << NoSpaceOnce() << ")";
}


template <>
void Log::append<const QObject*>(const QObject * const & object) noexcept
{
	if (object == nullptr) {
		appendString("QObject{null}");
		return;
	}

	append<QObject>(*object);
}


template <>
void Log::append<QObject>(const QObject & object) noexcept
{
	*this << "QObject{" << Log::NoSpaceOnce()
			<< Log::squared(Log::address(&object)) << Log::SpaceOnce()
			<< "type=" << Log::NoSpaceOnce() << object.metaObject()->className()
			<< "name=" << Log::NoSpaceOnce() << object.objectName()
			<< Log::NoSpaceOnce() << "}";
}


FLAT_DEBUG_LOG_APPEND_FROM_QT(QStringRef)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QStringList)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QPoint)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QSize)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QRect)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QPointF)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QSizeF)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QRectF)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QVariant)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QUrl)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QJsonValue)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QModelIndex)
FLAT_DEBUG_LOG_APPEND_FROM_QT(QDir)




#ifdef QT_NETWORK_LIB

FLAT_DEBUG_LOG_APPEND_FROM_QT(QHostAddress)

#endif




#ifdef QT_GUI_LIB

template <>
void Log::append<QFont::Style>(const QFont::Style & fontStyle) noexcept
{
	switch (fontStyle) {
	case QFont::StyleNormal:  appendString("Normal"); return;
	case QFont::StyleItalic:  appendString("Italic"); return;
	case QFont::StyleOblique: appendString("Oblique"); return;
	}

	printf("(Invalid font style: %d)", uint32_t(fontStyle));
	maybeSpace();
}


template <>
void Log::append<QFont>(const QFont & font) noexcept
{
	State state(*this);
	FLAT_UNUSED(state)

	maybeSpace();
	*this << NoSpace();

	*this << "QFont(" << qPrintable(font.family());

	if (font.pointSize() == -1) {
		*this << ", pixelSize:" << font.pixelSize();
	} else if ( font.pixelSize() == -1 ) {
		*this << ", pointSize:" << font.pointSizeF();
	}

	*this
			<< ", weight:" << font.weight()
			<< ", style:" << font.style()
			<< ", underline:" << font.underline()
			<< ", strikeout:" << font.strikeOut()
			<< ")";
}


template <>
void Log::append<QColor>(const QColor & color) noexcept
{
	printf("QColor(%d,%d,%d,%d)", color.red(), color.green(), color.blue(), color.alpha());
	maybeSpace();
}


template <>
void Log::append<QScreen>(const QScreen & screen) noexcept
{
	State state(*this);
	FLAT_UNUSED(state)

	maybeSpace();
	*this << NoSpace();

	*this << "QScreen("
			"name: " << screen.name()
			<< ", size:" << screen.size()
			<< ", psize:" << screen.physicalSize()
			<< ", pdpy:" << screen.physicalDotsPerInch()
			<< " (" << screen.physicalDotsPerInchX() << " x " << screen.physicalDotsPerInchY() << ")"
			<< ", ldpy:" << screen.logicalDotsPerInch()
			<< " (" << screen.logicalDotsPerInchX() << " x " << screen.logicalDotsPerInchY() << ")"
			<< ")";
}


template <>
void Log::append<QVector3D>(const QVector3D & vector) noexcept
{
	State state(*this);
	FLAT_UNUSED(state)

	maybeSpace();
	*this << NoSpace();

	*this << "QVector3D(" << vector.x() << " " << vector.y() << " " << vector.z() << ")";
}

#endif




#ifdef QT_QML_LIB

FLAT_DEBUG_LOG_APPEND_FROM_QT(QQmlError)

#endif




}}
