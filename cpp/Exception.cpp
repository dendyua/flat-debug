
#include "Exception.hpp"




namespace Flat {
namespace Debug {




void ExceptionCallback::_write(Log & log) noexcept
{
	body_ += std::string(log.buffer(), size_t(log.bufferSize()));
}


void ExceptionCallback::_flushed(Log &)
{
	std::rethrow_exception(exception_);
}




}}
