
#include "Offset.hpp"

#ifdef __linux__
#include <sys/syscall.h>
#endif

#include <thread>

#include <Flat/Core/Utils.hpp>




namespace Flat {
namespace Debug {




// shared const buffer with spaces for indentation string
extern const char sIndentStringBuffer[] =
		"                                                                "
		"                                                                ";
static_assert(sizeof(sIndentStringBuffer) - 1 == kIndentStringMaxLength, "Does not match");




static thread_local int callOffset = 0;




Offset Offset::sInstance;


Offset::Offset() noexcept
{
}


int Offset::valueForCurrentThread() noexcept
{
	return callOffset;
}


void Offset::_addValueForCurrentThread(int offset) noexcept
{
	callOffset += offset;
}




}}
