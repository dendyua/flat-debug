
#include "Fatal.hpp"




namespace Flat {
namespace Debug {




FatalLog::FatalLog(const char * const tag, const OutputMode outputMode) noexcept :
	Log(tag, Priority::Fatal, outputMode)
{
}

FatalLog::~FatalLog()
{
	flush();
	std::abort();
}




}}
