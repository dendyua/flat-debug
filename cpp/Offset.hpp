
#pragma once

#include <algorithm>




namespace Flat { namespace Debug {




static constexpr int kIndentStringMaxLength = 128;
extern FLAT_DEBUG_EXPORT const char sIndentStringBuffer[];




template <int IndentSize>
class IndentString
{
public:
	IndentString(int length) noexcept :
		offset_(std::max(kIndentStringMaxLength - length*IndentSize, 0))
	{
	}

	const char * string() const noexcept
	{
		return sIndentStringBuffer + offset_;
	}

private:
	const int offset_;
};




class FLAT_DEBUG_EXPORT Offset
{
public:
	static Offset * instance() noexcept
	{
		return &sInstance;
	}

	int valueForCurrentThread() noexcept;

	void increaseValueForCurrentThread() noexcept { _addValueForCurrentThread(1); }
	void decreaseValueForCurrentThread() noexcept { _addValueForCurrentThread(-1); }

private:
	Offset() noexcept;

	void _addValueForCurrentThread(int offset) noexcept;

private:
	static Offset sInstance;
};




}} // Flat::Debug
