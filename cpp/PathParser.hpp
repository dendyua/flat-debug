
#pragma once

#include <stdexcept>
#include <string>

#include <Flat/Core/Utils.hpp>




namespace Flat {
namespace Debug {




enum ParseMode
{
	ParseMode_Unix,
	ParseMode_Win
};




constexpr char to_lower(char c)
{
	return c >= 65 && c <= 90 ? c + 32 : c;
}


constexpr ParseMode kPlatformParseMode =
#ifdef FLAT_DEBUG_HOST_UNIX
		ParseMode_Unix;
#else
		ParseMode_Win;
#endif

constexpr bool is_slash(ParseMode mode, char ch)
{
	return mode == ParseMode_Unix ?
			ch == '/' :
			ch == '/' || ch == '\\';
}

constexpr bool is_absolute_path(ParseMode mode, const char * path)
{
	return mode == ParseMode_Unix ?
			is_slash(mode, path[0]) :
			stringLength(path) < 3 ? false : path[1] == ':' && is_slash(mode, path[2]);
}

constexpr int absolute_path_start_pos(ParseMode mode)
{
	return mode == ParseMode_Unix ? 1 : 3;
}

constexpr bool is_same_path_chars(ParseMode mode, char a, char b)
{
	return (is_slash(mode, a) && is_slash(mode, b)) || (mode == ParseMode_Unix ? a == b : to_lower(a) == to_lower(b));
}




struct Gap
{
	constexpr Gap() = default;
	constexpr Gap(int start, int end) : start(start), end(end) {}

	int start{-1}, end{-1};

	constexpr Gap uniteWith(Gap gap) const
	{
		return start == -1 ? gap : Gap(start, gap.start <= end ? gap.end : end);
	}
};




constexpr bool check_pos(ParseMode mode, int pos)
{
	return pos < absolute_path_start_pos(mode)-1 ? throw std::logic_error("pos < 0") : true;
}

constexpr int locate_gap_start(ParseMode mode, const char * p, int i, int depth = 0, bool atDirBegin = true)
{
	return
		depth == -1 && (atDirBegin ? throw std::logic_error("depth == -1 && atDirBegin") : true) ? i+1 :
		i == absolute_path_start_pos(mode)-1 && (depth != 0 || !atDirBegin ? throw std::logic_error("Unexpected line start") : true) ? absolute_path_start_pos(mode)-1 :
		atDirBegin ?
			is_slash(mode, p[i-1]) ? locate_gap_start(mode, p, i - 1, depth, true) :
			p[i-1] == '.' && check_pos(mode, i-2) ?
				is_slash(mode, p[i-2]) ? locate_gap_start(mode, p, i-2, depth, true) :
				p[i-2] == '.' && check_pos(mode, i-3) ?
					is_slash(mode, p[i-3]) ? locate_gap_start(mode, p, i-3, depth+1, true) :
					locate_gap_start(mode, p, i-3, depth-1, false) :
				locate_gap_start(mode, p, i-2, depth-1, false) :
			locate_gap_start(mode, p, i-1, depth-1, false) :

	// !atDirBegin
		is_slash(mode, p[i-1]) ? locate_gap_start(mode, p, i-1, depth, true) :
			locate_gap_start(mode, p, i-1, depth, false);
}


constexpr int stop_end(int end)
{
	return end == -1 ?
		throw std::logic_error(std::string()) : end;
		//-1 : end;
}

constexpr int locate_gap_end(ParseMode mode, const char * p, int size, int i, int end = -1, bool atDirBegin = true)
{
	return
		i == size ? end :

		atDirBegin ?
			is_slash(mode, p[i]) ? locate_gap_end(mode, p, size, i+1, i) :
			p[i] == '.'? i+1 == size ? i+1 :
				p[i+1] == '.' ? i+2 == size ? i+2 :
					is_slash(mode, p[i+2]) ? locate_gap_end(mode, p, size, i+3, i+2, true) :
					end != -1 ? stop_end(end) : locate_gap_end(mode, p, size, i+3, -1, false) :
				is_slash(mode, p[i+1]) ? locate_gap_end(mode, p, size, i+2, i+1, true) :
				end != -1 ? stop_end(end) : locate_gap_end(mode, p, size, i+2, -1, false) :
			end != -1 ? stop_end(end) : locate_gap_end(mode, p, size, i+1, -1, false) :

		// !atDirBegin
		is_slash(mode, p[i]) ? locate_gap_end(mode, p, size, i+1, end, true) :
		locate_gap_end(mode, p, size, i+1, end, false);
}


constexpr Gap locate_gap_imp2(ParseMode mode, const char * p, int size, int i, Gap gap, Gap currentGap, bool gapEndChecked = false)
{
	return
		gapEndChecked && currentGap.end == -1 ? gap :
		i >= size ? gap :
		currentGap.start != -1 ?  locate_gap_imp2(mode, p, size, currentGap.end+1, gap.uniteWith(currentGap), Gap(), false) :
		currentGap.end != -1 ? locate_gap_imp2(mode, p, size, i, gap, Gap(locate_gap_start(mode, p, currentGap.end), currentGap.end), true) :
		locate_gap_imp2(mode, p, size, i, gap, Gap(-1, locate_gap_end(mode, p, size, i)), true);
}


constexpr Gap locate_gap(ParseMode mode, const char * p)
{
	return
		(is_absolute_path(mode, p) ? true : throw std::logic_error("")) ?
		locate_gap_imp2(mode, p, stringLength(p), 1, Gap(), Gap()) :
		Gap();
}




constexpr bool compare_paths_imp3(ParseMode mode, const char * referencePath, int referenceSize, const char * path, int size, Gap gap, int ri = 0, int i = 0)
{
	return
		ri == referenceSize && i == size ? true :
		gap.start == -1 || i < gap.start ? referencePath[ri] != path[i] ? false :
			compare_paths_imp3(mode, referencePath, referenceSize, path, size, gap, ri+1, i+1) :
		compare_paths_imp3(mode, referencePath, referenceSize, path, size, locate_gap_imp2(mode, path, size, gap.end+1, Gap(), Gap()), ri, gap.end);
}

constexpr bool compare_paths_imp2(ParseMode mode, const char * referencePath, int referenceSize, const char * path, int size)
{
	return compare_paths_imp3(mode, referencePath, referenceSize, path, size, locate_gap_imp2(mode, path, size, absolute_path_start_pos(mode), Gap(), Gap()));
}

constexpr bool compare_paths(ParseMode mode, const char * referencePath, const char * path)
{
	return compare_paths_imp2(mode, referencePath, stringLength(referencePath), path, stringLength(path));
}




constexpr int _starts_with_pos_imp_stop(int pos)
{
	return
		//pos == -1 || pos >= 0 ? throw std::logic_error(std::string()) : pos;
		pos;
}

constexpr char _starts_with_pos_imp_dir_at(const char * dir, int dirSize, int i)
{
	return i == dirSize ? '/' : dir[i];
}

constexpr int _starts_with_pos_imp2(ParseMode mode, const char * dir, int dirSize, const char * path, int pathSize, Gap gap, int di = 0, int pi = 0)
{
	return
		pi == pathSize ? di == dirSize || di == dirSize+1 ? pi : _starts_with_pos_imp_stop(-1) :
		di == dirSize+1 ? pi :
		gap.start == -1 || pi < gap.start ? !is_same_path_chars(mode, _starts_with_pos_imp_dir_at(dir, dirSize, di), path[pi]) ? _starts_with_pos_imp_stop(-1) :
			_starts_with_pos_imp2(mode, dir, dirSize, path, pathSize, gap, di+1, pi+1) :
		_starts_with_pos_imp2(mode, dir, dirSize, path, pathSize, locate_gap_imp2(mode, path, pathSize, gap.end+1, Gap(), Gap()), di, gap.end);
}

constexpr int _starts_with_pos_imp(ParseMode mode, const char * dir, int dirSize, const char * path, int pathSize)
{
	return _starts_with_pos_imp2(mode, dir, dirSize, path, pathSize, locate_gap_imp2(mode, path, pathSize, absolute_path_start_pos(mode), Gap(), Gap()));
}

constexpr int starts_with_pos(ParseMode mode, const char * dir, const char * path)
{
	return !is_absolute_path(mode, dir) || !is_absolute_path(mode, path) ? -1 : _starts_with_pos_imp(mode, dir, stringLength(dir), path, stringLength(path));
}

constexpr bool starts_with(ParseMode mode, const char * dir, const char * path)
{
	return starts_with_pos(mode, dir, path) != -1;
}




class ParsePathIt
{
public:
	static constexpr ParsePathIt create(ParseMode mode, const char * s, int size = -1)
	{
		return _create_1(mode, s, size == -1 ? stringLength(s) : size);
	}

	static constexpr char unifiedWindowsValue(const char ch)
	{
		return is_slash(ParseMode_Win, ch) ? '/' : to_lower(ch);
	}

	constexpr ParseMode mode() const
	{
		return mode_;
	}

	constexpr const char * string() const
	{
		return s_;
	}

	constexpr const char * currentString() const
	{
		return s_ + i_;
	}

	constexpr int size() const
	{
		return size_;
	}

	constexpr char value() const
	{
		return mode_ == ParseMode_Unix ? s_[i_] : unifiedWindowsValue(s_[i_]);
	}

	constexpr int pos() const
	{
		return pos_;
	}

	constexpr bool atEnd() const
	{
		return gap_.start != -1 && gap_.end == size_ ? i_ == gap_.start : i_ == size_;
	}

	constexpr ParsePathIt next() const
	{
		return
			atEnd() ? throw std::logic_error("Iterating over end") :
			_next_2();
	}

	constexpr ParsePathIt truncated() const
	{
		return create(mode_, s_, pos_);
	}

	constexpr ParsePathIt(const ParsePathIt & other) = default;

private:
	constexpr ParsePathIt(ParseMode mode, const char * s, int size, Gap gap, int i, int pos) :
		mode_(mode),
		s_(s),
		size_(size),
		gap_(gap),
		i_(i),
		pos_(pos)
	{}

private:
	static constexpr ParsePathIt _create_1(ParseMode mode, const char * s, int size)
	{
		return _create_2(mode, s, size,
				locate_gap_imp2(mode, s, size, absolute_path_start_pos(mode), Gap(), Gap()), 0, 0);
	}

	static constexpr ParsePathIt _create_2(ParseMode mode, const char * s, int size, Gap gap, int i, int pos)
	{
		return gap.start == -1 || gap.start > 0 ? ParsePathIt(mode, s, size, gap, i, pos) :
			_create_2(mode, s, size, locate_gap_imp2(mode, s, size, gap.end + 1, Gap(), Gap()), gap.end, pos);
	}

	constexpr ParsePathIt _next_2() const
	{
		return gap_.start == -1 || i_ < gap_.start ? ParsePathIt(mode_, s_, size_, gap_, i_ + 1, pos_ + 1) :
				_create_2(mode_, s_, size_, locate_gap_imp2(mode_, s_, size_, gap_.end + 1, Gap(), Gap()), gap_.end + 1, pos_ + 1);
	}

private:
	ParseMode mode_;
	const char * s_;
	int size_;
	Gap gap_;
	int i_;
	int pos_;
};




constexpr int _path_length_imp(ParsePathIt it, int length)
{
	return it.atEnd() ? length : _path_length_imp(it.next(), length + 1);
}

constexpr int path_length(const char * path)
{
	return _path_length_imp(ParsePathIt::create(ParseMode_Win, path), 0);
}




constexpr const char * _back_shift_to_basename(ParseMode mode, const char * path)
{
	return is_slash(mode, path[0]) ? path + 1 : _back_shift_to_basename(mode, path - 1);
}


constexpr const char * _shifted_path_imp(ParsePathIt it, int offset)
{
	if (it.pos() == offset) {
		return it.currentString();
	}

	if (it.atEnd()) {
		return _back_shift_to_basename(it.mode(), it.currentString());
	}

	return _shifted_path_imp(it.next(), offset);
}


constexpr const char * shifted_path(const char * path, int offset)
{
	return _shifted_path_imp(ParsePathIt::create(kPlatformParseMode, path), offset);
}




}}
