
#pragma once

#include "Log.hpp"




namespace Flat {
namespace Debug {




class FLAT_DEBUG_EXPORT FatalLog : public Log
{
public:
	FatalLog(const char * tag, OutputMode outputMode) noexcept;
	[[noreturn]] ~FatalLog();
};




}}
