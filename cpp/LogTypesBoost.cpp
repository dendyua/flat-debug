
#include "LogTypesBoost.hpp"




namespace Flat {
namespace Debug {




struct StrPrinter {
	template <typename T>
	static void print(Log & log, T str, const boost::filesystem::path & path);
};

template <>
void StrPrinter::print<const char *>(Log & log, const char * const str,
		const boost::filesystem::path &) {
	log.appendString(str);
};

template <>
void StrPrinter::print<const wchar_t *>(Log & log, const wchar_t *,
		const boost::filesystem::path & path) {
	// FIXME: string() method is expensive.
	log.appendString(path.string().c_str());
};




template <>
void Log::append<boost::filesystem::path>(
		const boost::filesystem::path & path) noexcept
{
	StrPrinter::print(*this, path.c_str(), path);
}


template <>
void Log::append<boost::filesystem::file_type>(
		const boost::filesystem::file_type & type) noexcept
{
#define FLAT_DEBUG_LOG_BOOST_FILE_TYPE(v) case boost::filesystem:: v: appendString(#v); return;

	switch (type) {
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(status_error)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(file_not_found)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(regular_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(directory_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(symlink_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(block_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(character_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(fifo_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(socket_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(reparse_file)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(type_unknown)
	FLAT_DEBUG_LOG_BOOST_FILE_TYPE(_detail_directory_symlink)
	}

	*this << "(Invalid file_type:" << int(type) << NoSpaceOnce() << ")";

#undef FLAT_DEBUG_LOG_BOOST_FILE_TYPE
}




}} // Flat::Debug
