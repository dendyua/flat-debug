
#pragma once

#include <QBuffer>
#include <QDataStream>
#include <QDebug>
#include <QEvent>
#include <QList>

#ifdef QT_GUI_LIB
#include <QFont>
#endif

#include "Log.hpp"




class QStringRef;
class QByteArray;
class QString;
class QStringList;
class QPoint;
class QSize;
class QRect;
class QPointF;
class QSizeF;
class QRectF;
class QVariant;
class QUrl;
class QJsonValue;
class QModelIndex;
class QObject;
class QDir;

#ifdef QT_NETWORK_LIB
class QHostAddress;
#endif

#ifdef QT_GUI_LIB
class QColor;
class QScreen;
class QVector3D;
#endif

#ifdef QT_QML_LIB
class QQmlError;
#endif




namespace Flat {
namespace Debug {




template <typename T>
inline void appendFromQt(Log & log, const T & value) noexcept
{
	QBuffer buffer;
	buffer.open(QIODevice::WriteOnly);
	{
		QDebug debug(&buffer);
		debug << value;
	}
	buffer.close();
	log.print(buffer.buffer().constData());
}

#define FLAT_DEBUG_LOG_APPEND_FROM_QT(T) \
	template <> void Log::append<T>(const T & value) noexcept { appendFromQt(*this, value); }




template <> FLAT_DEBUG_QT_EXPORT void Log::append<QStringRef>(const QStringRef & byteArray) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QByteArray>(const QByteArray & byteArray) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QString>(const QString & string) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QStringList>(
		const QStringList & stringList) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QPoint>(const QPoint & point) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QSize>(const QSize & size) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QRect>(const QRect & rect) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QPointF>(const QPointF & point) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QSizeF>(const QSizeF & size) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QRectF>(const QRectF & rect) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QDataStream::Status>(
		const QDataStream::Status & status) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QEvent::Type>(const QEvent::Type & type) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QVariant>(const QVariant & value) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QUrl>(const QUrl & url) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QJsonValue>(const QJsonValue & jsonValue) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QModelIndex>(const QModelIndex & index) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<const QObject*>(
		const QObject * const & object) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QObject>(const QObject & object) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QDir>(const QDir & object) noexcept;

#ifdef QT_NETWORK_LIB
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QHostAddress>(
		const QHostAddress & address) noexcept;
#endif

#ifdef QT_GUI_LIB
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QFont::Style>(
		const QFont::Style & fontStyle) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QFont>(const QFont & font) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QColor>(const QColor & color) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QScreen>(const QScreen & screen) noexcept;
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QVector3D>(const QVector3D & vector) noexcept;
#endif

#ifdef QT_QML_LIB
template <> FLAT_DEBUG_QT_EXPORT void Log::append<QQmlError>(const QQmlError & error) noexcept;
#endif




template <typename T>
inline void operator<<(Log::Append & append, const QList<T> & list) noexcept
{
	append.log() << "(";
	for (int i = 0; i < list.count(); ++i) {
		if (i != 0) {
			append.log() << Log::NoSpaceOnce() << ", ";
		}
		append.log() << list.at(i) << Log::NoSpaceOnce();
	}
	append.log() << ")";
}




}}
