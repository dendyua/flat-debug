
#pragma once

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include "Log.hpp"




namespace Flat {
namespace Debug {




template <> FLAT_DEBUG_BOOST_EXPORT void Log::append<boost::filesystem::path>(
		const boost::filesystem::path & path) noexcept;

template <> FLAT_DEBUG_BOOST_EXPORT void Log::append<boost::filesystem::file_type>(
		const boost::filesystem::file_type & type) noexcept;




}}
