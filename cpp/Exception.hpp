
#pragma once

#include <stdexcept>
#include <string>
#include <string_view>

#include "Log.hpp"




namespace Flat {
namespace Debug {




struct ExceptionContext
{
	std::string_view module;
	std::string_view file;
	int line;
	std::string_view function;
};




class FLAT_DEBUG_EXPORT Error : public std::runtime_error
{
public:
	Error(const std::string & what = {}) noexcept :
		std::runtime_error(what)
	{}

	~Error() noexcept override = default;

	const ExceptionContext & context() const noexcept { return context_; }

private:
	ExceptionContext context_;

	template <typename E>
	friend std::exception_ptr makeExceptionPointerFromError(E e, const ExceptionContext & context) noexcept;
};




class FLAT_DEBUG_EXPORT ExceptionCallback
{
public:
	class Set
	{
	public:
		Set(std::exception_ptr && exception) noexcept : exception(exception) {}

		mutable std::exception_ptr exception;
	};

	class OutputCallback : public Log::OutputCallback
	{
	public:
		OutputCallback(const std::shared_ptr<ExceptionCallback> & exceptionCallback) noexcept :
			exceptionCallback(exceptionCallback)
		{}

		void write(Log & log) noexcept override { exceptionCallback->_write(log); }

		const std::shared_ptr<ExceptionCallback> exceptionCallback;
	};

	class FlushCallback : public Log::FlushCallback
	{
	public:
		FlushCallback(const std::shared_ptr<ExceptionCallback> & exceptionCallback) noexcept :
			exceptionCallback(exceptionCallback)
		{}

		void flushed(Log & log) override { exceptionCallback->_flushed(log); }

		const std::shared_ptr<ExceptionCallback> exceptionCallback;
	};

	ExceptionCallback(std::exception_ptr && exception) noexcept :
		exception_(exception)
	{}

private:
	void _write(Log & log) noexcept;
	[[noreturn]] void _flushed(Log & log);

private:
	const std::exception_ptr exception_;
	std::string body_;
};




template <typename E>
inline std::exception_ptr makeExceptionPointerFromError(E e, const ExceptionContext & context) noexcept
{
	static_assert(std::is_base_of_v<Error, E>, "");
	static_cast<Error&>(e).context_ = context;
	return std::make_exception_ptr(e);
}




template <>
inline void Log::append<ExceptionCallback::Set>(const ExceptionCallback::Set & set) noexcept
{
	const std::shared_ptr<ExceptionCallback> c =
			std::make_shared<ExceptionCallback>(std::move(set.exception));
	*this << SetOutputCallback(std::make_shared<ExceptionCallback::OutputCallback>(c));
	*this << SetFlushCallback(std::make_shared<ExceptionCallback::FlushCallback>(c));
}




}}
