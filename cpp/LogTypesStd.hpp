
#pragma once

#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <variant>
#include <vector>

#include "Log.hpp"




namespace Flat {
namespace Debug {




template <> FLAT_DEBUG_EXPORT void Log::append<std::string>(const std::string & string) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<std::string_view>(const std::string_view & string) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<std::exception>(
		const std::exception & exception) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<std::runtime_error>(
		const std::runtime_error & exception) noexcept;
template <> FLAT_DEBUG_EXPORT void Log::append<std::nullptr_t>(const std::nullptr_t &) noexcept;




template <typename T>
inline void operator<<(Log::Append & append, const std::vector<T> & list) noexcept
{
	append.log() << "(";
	for (int i = 0; i < static_cast<int>(list.size()); ++i) {
		if (i != 0) {
			append.log() << Log::NoSpaceOnce() << ", ";
		}
		append.log() << list[i] << Log::NoSpaceOnce();
	}
	append.log() << ")";
}




template <typename T>
inline void operator<<(Log::Append & append, const std::unique_ptr<T> & ptr) noexcept
{
	append.log() << ptr.get();
}




template <typename T>
inline void operator<<(Log::Append & append, const std::optional<T> & o) noexcept
{
	if (o.has_value()) {
		append.log() << o.value();
	} else {
		append.log() << "<nul>";
	}
}




template <typename... T>
inline void operator<<(Log::Append & append, const std::variant<T...> & v) noexcept
{
	std::visit([&] (auto && p) {
		append.log() << p;
	}, v);
}




}}
